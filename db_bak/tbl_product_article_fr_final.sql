-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 17, 2015 at 04:05 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `catalog_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_article_fr_final`
--

CREATE TABLE IF NOT EXISTS `tbl_product_article_fr_final` (
  `user_id` int(10) NOT NULL,
  `product_article_id` int(10) NOT NULL AUTO_INCREMENT,
  `article_number` int(10) NOT NULL,
  `ean_code` int(15) NOT NULL,
  `article_brand` varchar(100) CHARACTER SET utf8 NOT NULL,
  `material_type` varchar(100) CHARACTER SET utf8 NOT NULL,
  `extra_info` varchar(100) CHARACTER SET utf8 NOT NULL,
  `product_name` varchar(150) CHARACTER SET utf8 NOT NULL,
  `weight` varchar(50) CHARACTER SET utf8 NOT NULL,
  `width` varchar(50) CHARACTER SET utf8 NOT NULL,
  `length` varchar(50) CHARACTER SET utf8 NOT NULL,
  `article_group` varchar(150) CHARACTER SET utf8 NOT NULL,
  `article_description` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `design_name` varchar(150) CHARACTER SET utf32 NOT NULL,
  `colors` varchar(150) CHARACTER SET utf8 NOT NULL,
  `unity` varchar(150) CHARACTER SET utf8 NOT NULL,
  `selling_unity` varchar(100) CHARACTER SET utf8 NOT NULL,
  `packing_unity` varchar(100) CHARACTER SET utf8 NOT NULL,
  `price` varchar(10) CHARACTER SET utf8 NOT NULL,
  `status` varchar(100) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`product_article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
