-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 15, 2015 at 04:57 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `catalog_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ci_sessions`
--

DROP TABLE IF EXISTS `tbl_ci_sessions`;
CREATE TABLE IF NOT EXISTS `tbl_ci_sessions` (
  `ai` bigint(20) unsigned NOT NULL,
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tbl_ci_sessions`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_denied_access`
--

DROP TABLE IF EXISTS `tbl_denied_access`;
CREATE TABLE IF NOT EXISTS `tbl_denied_access` (
  `ai` int(10) unsigned NOT NULL,
  `IP_address` varchar(45) NOT NULL,
  `time` datetime NOT NULL,
  `reason_code` tinyint(2) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tbl_denied_access`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ips_on_hold`
--

DROP TABLE IF EXISTS `tbl_ips_on_hold`;
CREATE TABLE IF NOT EXISTS `tbl_ips_on_hold` (
  `ai` int(10) unsigned NOT NULL,
  `IP_address` varchar(45) NOT NULL,
  `time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tbl_ips_on_hold`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login_errors`
--

DROP TABLE IF EXISTS `tbl_login_errors`;
CREATE TABLE IF NOT EXISTS `tbl_login_errors` (
  `ai` int(10) unsigned NOT NULL,
  `username_or_email` varchar(255) NOT NULL,
  `IP_address` varchar(45) NOT NULL,
  `time` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tbl_login_errors`:
--

--
-- Dumping data for table `tbl_login_errors`
--

INSERT INTO `tbl_login_errors` (`ai`, `username_or_email`, `IP_address`, `time`) VALUES
(6, 'nazmul', '::1', '2015-08-14 16:56:27');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_article`
--

DROP TABLE IF EXISTS `tbl_product_article`;
CREATE TABLE IF NOT EXISTS `tbl_product_article` (
  `prod_article_id` int(16) NOT NULL,
  `user_Id` int(11) DEFAULT NULL,
  `brand` varchar(100) DEFAULT NULL,
  `article_group` varchar(100) DEFAULT NULL,
  `design_group` varchar(100) DEFAULT NULL,
  `article_number` int(16) DEFAULT NULL,
  `article_price` int(16) DEFAULT NULL,
  `design_name` varchar(200) DEFAULT NULL,
  `ean_code` varchar(255) DEFAULT NULL,
  `color_setting` varchar(100) DEFAULT NULL,
  `selling_unity` varchar(100) DEFAULT NULL,
  `measurement` varchar(100) DEFAULT NULL,
  `packing_unity` varchar(100) DEFAULT NULL,
  `length` int(12) DEFAULT NULL,
  `height` int(20) DEFAULT NULL,
  `width` int(20) DEFAULT NULL,
  `weight` int(20) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `design_picture` varchar(255) DEFAULT NULL,
  `mapping_picture` varchar(255) DEFAULT NULL,
  `article_description_de` text,
  `article_description_nl` text,
  `article_description_fr` text,
  `article_description_en` text,
  `insert_date` datetime DEFAULT NULL,
  `last_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tbl_product_article`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_site_config`
--

DROP TABLE IF EXISTS `tbl_site_config`;
CREATE TABLE IF NOT EXISTS `tbl_site_config` (
  `id` int(11) NOT NULL,
  `variable` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `descriptions` varchar(255) NOT NULL,
  `year` int(4) unsigned NOT NULL DEFAULT '0',
  `variablenote` text
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tbl_site_config`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_username_or_email_on_hold`
--

DROP TABLE IF EXISTS `tbl_username_or_email_on_hold`;
CREATE TABLE IF NOT EXISTS `tbl_username_or_email_on_hold` (
  `ai` int(10) unsigned NOT NULL,
  `username_or_email` varchar(255) NOT NULL,
  `time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tbl_username_or_email_on_hold`:
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `user_id` int(10) unsigned NOT NULL,
  `user_name` varchar(12) DEFAULT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_pass` varchar(60) NOT NULL,
  `user_salt` varchar(32) NOT NULL,
  `user_last_login` datetime DEFAULT NULL,
  `user_login_time` datetime DEFAULT NULL,
  `user_session_id` varchar(40) DEFAULT NULL,
  `user_date` datetime NOT NULL,
  `user_modified` datetime NOT NULL,
  `user_agent_string` varchar(32) DEFAULT NULL,
  `user_level` tinyint(2) unsigned NOT NULL,
  `user_banned` enum('0','1') NOT NULL DEFAULT '0',
  `passwd_recovery_code` varchar(60) DEFAULT NULL,
  `passwd_recovery_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tbl_users`:
--

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `user_name`, `user_email`, `user_pass`, `user_salt`, `user_last_login`, `user_login_time`, `user_session_id`, `user_date`, `user_modified`, `user_agent_string`, `user_level`, `user_banned`, `passwd_recovery_code`, `passwd_recovery_date`) VALUES
(0, 'admin', '', '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0, '0', NULL, NULL),
(122, 'nazmul', 'freelancerplanetbd@gmail.com', '$2a$09$b35f309eb4a4dcb697b14uPQ6Spkey2pypDqrNtF2Iic7z1AcYUsm', 'b35f309eb4a4dcb697b14021dede08ab', '2015-08-15 04:39:42', '2015-08-15 04:39:42', 'ed1af2566e02a1b4316fb6b227eac0a3688201d9', '2015-08-12 15:13:36', '2015-08-12 15:13:36', '39da06a1469a2a2f873f7659ce753545', 9, '0', NULL, NULL),
(534, 'skunkbot', 'skunkbot@example.com', '$2a$09$b7ec84a004c5d81daf02du8pNY6BotRcab6UqO0Lc3uCIWXnz85Gy', 'b7ec84a004c5d81daf02d24b41faa761', '2015-08-12 15:11:38', '2015-08-12 15:11:38', '9dda9bfcb8a164d77107d585114b606623890b3f', '2015-08-12 15:06:11', '2015-08-12 15:06:11', '1b1a9fe8dea603b93684d610b5808c02', 1, '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users_details`
--

DROP TABLE IF EXISTS `tbl_users_details`;
CREATE TABLE IF NOT EXISTS `tbl_users_details` (
  `users_details_id` int(12) NOT NULL,
  `user_id` int(12) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `tel` int(20) DEFAULT NULL,
  `mobile` int(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `postal_code` varchar(50) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `province` varchar(100) DEFAULT NULL,
  `contacct_details` varchar(255) DEFAULT NULL,
  `notes` text,
  `owned_company` varchar(255) DEFAULT NULL,
  `default_language` varchar(5) DEFAULT NULL,
  `purchase_central` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `client_type` varchar(100) DEFAULT NULL,
  `shop_type` varchar(100) DEFAULT NULL,
  `client_short_name` varchar(100) DEFAULT NULL,
  `verification_code` varchar(100) DEFAULT NULL,
  `company_name` varchar(100) DEFAULT NULL,
  `insert_date` datetime DEFAULT NULL,
  `last_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `tbl_users_details`:
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_ci_sessions`
--
ALTER TABLE `tbl_ci_sessions`
  ADD PRIMARY KEY (`ai`), ADD UNIQUE KEY `ci_sessions_id_ip` (`id`,`ip_address`), ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `tbl_denied_access`
--
ALTER TABLE `tbl_denied_access`
  ADD PRIMARY KEY (`ai`);

--
-- Indexes for table `tbl_ips_on_hold`
--
ALTER TABLE `tbl_ips_on_hold`
  ADD PRIMARY KEY (`ai`);

--
-- Indexes for table `tbl_login_errors`
--
ALTER TABLE `tbl_login_errors`
  ADD PRIMARY KEY (`ai`);

--
-- Indexes for table `tbl_product_article`
--
ALTER TABLE `tbl_product_article`
  ADD PRIMARY KEY (`prod_article_id`), ADD KEY `user_Id` (`user_Id`,`brand`,`article_group`,`design_group`,`article_number`);

--
-- Indexes for table `tbl_site_config`
--
ALTER TABLE `tbl_site_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_username_or_email_on_hold`
--
ALTER TABLE `tbl_username_or_email_on_hold`
  ADD PRIMARY KEY (`ai`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`), ADD UNIQUE KEY `user_email` (`user_email`), ADD UNIQUE KEY `user_name` (`user_name`);

--
-- Indexes for table `tbl_users_details`
--
ALTER TABLE `tbl_users_details`
  ADD PRIMARY KEY (`users_details_id`), ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_ci_sessions`
--
ALTER TABLE `tbl_ci_sessions`
  MODIFY `ai` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_denied_access`
--
ALTER TABLE `tbl_denied_access`
  MODIFY `ai` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_ips_on_hold`
--
ALTER TABLE `tbl_ips_on_hold`
  MODIFY `ai` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_login_errors`
--
ALTER TABLE `tbl_login_errors`
  MODIFY `ai` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_product_article`
--
ALTER TABLE `tbl_product_article`
  MODIFY `prod_article_id` int(16) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_site_config`
--
ALTER TABLE `tbl_site_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_username_or_email_on_hold`
--
ALTER TABLE `tbl_username_or_email_on_hold`
  MODIFY `ai` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users_details`
--
ALTER TABLE `tbl_users_details`
  MODIFY `users_details_id` int(12) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
