<?php
  class User_details_model extends My_Model{
  	function __construct(){
  		parent::__construct();
		$this->loadTable('tbl_users_details');
  	}
  	
  	 function save($op=NULL,$where=array())
   {
        $model_for="User details ";
		$return=(object)array('status'=>false,'data'=>'Error Occured');
		$this->create();
		$posted_data=$this->input->post();
		$data=array();
		if(isset($posted_data) && is_array($posted_data) && count($posted_data))
		{
		   foreach($this->input->post() as $field=>$value)
		   {
		      $data[trim($field)]=(is_array($value) || is_object($value)) ? $value : trim($value);
		   }
		}
		switch($op)
		{
		  case 'add':
		    $this->data=$data;
			if($this->insert())
			{
			  $return->status=true;
			  $return->data=$model_for."added successfully";
			}
		  break;
		  case 'edit':
		    $this->data=$data;   
			$this->where=$where;
		    if($this->update())
			{
			  $return->status=true;
			  $return->data=$model_for."edited successfully";
			}
		  break;
		  case 'del':
		     $this->primaryKey='id';
			 $this->id=$where['id'];
			 $this->remove();
			 $return->status=true;
			 $return->data=$model_for."removed successfully";
		  
		  break;
		  default:
		  break;
		}
	    return $return;	
    } 
	
}
?>