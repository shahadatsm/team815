<?php
  class Catalog_maker_model extends My_Model{
  	function __construct(){
  		parent::__construct();
		//$this->loadTable('tbl_product_article');
  	}
  	
  	 function save($op=NULL,$where=array())
   {
        $model_for="User ";
		$return=(object)array('status'=>false,'data'=>'Error Occured');
		$this->create();
		$posted_data=$this->input->post();
		$data=array();
		if(isset($posted_data) && is_array($posted_data) && count($posted_data))
		{
		   foreach($this->input->post() as $field=>$value)
		   {
		      $data[trim($field)]=(is_array($value) || is_object($value)) ? $value : trim($value);
		   }
		}
		$data['user_salt']     = $this->authentication->random_salt();
		$data['user_pass']     = $this->authentication->hash_passwd($data['user_pass'], $data['user_salt']);
		$data['user_id']       = $this->_get_unused_id();
		$data['user_date']     = date('Y-m-d H:i:s');
		$data['user_modified'] = date('Y-m-d H:i:s');	
			
		switch($op)
		{
		  case 'add':
		    $this->data=$data;
			if($this->insert())
			{
			  $return->status=true;
			  $return->data=$model_for."added successfully";
			}
		  break;
		  case 'edit':
		    $this->data=$data;   
			$this->where=$where;
		    if($this->update())
			{
			  $return->status=true;
			  $return->data=$model_for."edited successfully";
			}
		  break;
		  case 'del':
		     $this->primaryKey='id';
			 $this->id=$where['id'];
			 $this->remove();
			 $return->status=true;
			 $return->data=$model_for."removed successfully";
		  
		  break;		  
		  default:
		  break;
		}
	    return $return;	
    } 
	
	public function gete_data_by_articleid($article_id = NULL, $lang = NULL)
	{
		$query = $this->db->from( 'tbl_product_article_'.trim($lang))
			->where('number', $article_id)
			->get();
		if( $query->num_rows() == 1 )
		{
			return $query->row();
		}

		return FALSE;						
	}
	
	public function get_user_id( $username, $email )
	{
		$query = $this->db->select('u.user_id, u.user_email')
			->from( config_item('user_table') . ' u')
			->where('u.user_id', $username)
			->or_where('u.user_email', $email)
			->get();

		if( $query->num_rows() == 1 )
		{
			return $query->row();
		}

		return FALSE;
	}//End get_user_id()
	
	 private function _get_unused_id()
    {
        // Create a random user id
        $random_unique_int = mt_rand(1200, 4294967295);

        // Make sure the random user_id isn't already in use
        $query = $this->db->where('user_id', $random_unique_int)->get_where(config_item('user_table'));

        if ($query->num_rows() > 0){
            $query->free_result();
            // If the random user_id is already in use, get a new number
            return $this->_get_unused_id();
        }

        return $random_unique_int;
    }
	
	public function check_userdb($user = NULL)
	{
		$query = $this->db->get_where(config_item('user_table'), array('user_name' => $user));
		if ($query->num_rows() > 0) {
			return FALSE;
		}
		else {
			return TRUE;
		}
		
	}//End check_userdb()

	public function check_emaildb($email = NULL)
	{
		$query = $this->db->get_where(config_item('user_table'), array('user_email' => $email));
		if ($query->num_rows() > 0) {
			return FALSE;
		}
		else {
			return TRUE;
		}		
	}//End check_emaildb()
	
}
?>