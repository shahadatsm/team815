<?php
class Catalog_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	
	
	public function get_user_id( $username, $email )
	{
		$query = $this->db->select('u.user_id, u.user_email')
			->from( config_item('user_table') . ' u')
			->where('u.user_id', $username)
			->or_where('u.user_email', $email)
			->get();

		if( $query->num_rows() == 1 )
		{
			return $query->row();
		}

		return FALSE;
	}//End get_user_id()
						
	
}//End Class Catalog_Model