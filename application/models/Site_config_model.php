<?php
class Site_config_model extends MY_Model{
   function __construct(){
  		parent::__construct();
		$this->loadTable('tbl_site_config');
   }
   function save($op=NULL,$post=array())
   {
		$return->error=true;
		$return->message='';
		
		$data['variable'] =  trim($this->input->post('variable'));
		$data['value'] =  trim($this->input->post('value'));
		$data['descriptions'] =  trim($this->input->post('descriptions'));
		$data['variablenote'] = trim($this->input->post('variablenote'));
		$data['year'] =  $this->input->post('year');
		if($op=='add')
		{
		  
		  $this->create();
		  $this->data=$data;

		  if($this->insert())
		  {
		    $return->error=false;
			$return->message='Variable added successfully';
		  }	
		}
		else if($op=='edit')
		{
		  $this->create();
		  $this->data=$data;

		  $this->where['id']=$this->input->post('id');
		   if($this->update())
		  {
		    $return->error=false;
			$return->message='Variable edited successfully';
		  }	
		}
		else if($op=='del')
		{
		  $this->primaryKey='id';
		  $this->id=$this->input->post('id');
		  $this->remove();
		  $return->error=false;
		  $return->message='Variable deleted successfully';
		}
	  return $return;	
   }
}
?>