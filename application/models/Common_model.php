<?php
  class Common_model extends CI_Model{
  	var $common_data=array();
	var $CI="";
  	function __construct(){
  		parent::__construct();
		$this->CI =& get_instance();
  	}
  	
  	function getPageCommonData()
	{
		$data = array();
		$data['login_html']="";
		$this->CI->load->model("site_config_model");
		$google_ua_code=$this->CI->site_config_model->find(array("variable"=>"google_ua_code"));
	    $data['lang_option']=$this->lang_options();
		$site_lang=$this->session->userdata("site_lang");
		$site_lang=($site_lang) ? $site_lang : 'en';
		//$this->config->set_item('language', $site_lang);
		$site_lang_text=$this->get_lang_text($site_lang);
		$data['site_lang']=$site_lang;
		$data['site_lang_text']=$site_lang_text;
		
		$data["google_ua_code"]=($google_ua_code) ? $google_ua_code['value'] : '';
		return $data;
  	}
	
 function sendemail($from_email=NULL,$from_name=NULL,$send_to_list=NULL,$reply_to_email=NULL,$reply_to_name=NULL,$subject=NULL,$message=NULL,$post_subject=" | Portal Issue",$attachment=NULL,$use_bcc=false)
	{
	    $config['useragent'] = 'test'; // The "user agent".
        $config['protocol'] = 'mail'; // mail, sendmail, or smtp    The mail sending protocol.
        $config['mailpath'] = '/usr/sbin/sendmail'; // The server path to Sendmail.
        $config['smtp_host'] = ''; // SMTP Server Address.
        $config['smtp_user'] = ''; // SMTP Username.
        $config['smtp_pass'] = ''; // SMTP Password.
        $config['smtp_port'] = '2525'; // SMTP Port.
        $config['smtp_timeout'] = '5'; // SMTP Timeout (in seconds).
        $config['wordwrap'] = TRUE; // TRUE or FALSE (boolean)    Enable word-wrap.
        $config['wrapchars'] = 76; // Character count to wrap at.
        $config['mailtype'] = 'html'; // text or html Type of mail. If you send HTML email you must send it as a complete web page. Make sure you don't have any relative links or relative image paths otherwise they will not work.
        $config['charset'] = 'utf-8'; // Character set (utf-8, iso-8859-1, etc.).
        $config['validate'] = FALSE; // TRUE or FALSE (boolean)    Whether to validate the email address.
        $config['priority'] = 3; // 1, 2, 3, 4, 5    Email Priority. 1 = highest. 5 = lowest. 3 = normal.
        $config['crlf'] = '\r\n'; // "\r\n" or "\n" or "\r" Newline character. (Use "\r\n" to comply with RFC 822).
        $config['newline'] = '\r\n'; // "\r\n" or "\n" or "\r"    Newline character. (Use "\r\n" to comply with RFC 822).
        $config['bcc_batch_mode'] = FALSE; // TRUE or FALSE (boolean)    Enable BCC Batch Mode.
        $config['bcc_batch_size'] = 200; // Number of emails in each BCC batch.
        $data=array();
        
		$this->load->library('email');
        $this->email->initialize($config);

        if(!$send_to_list) 
         $send_to_list = array('support@unicornranch.me');
        if(!$from_email) 
          $from_email = 'support@unicornranch.me'; 
        if(!$from_name) 
         $from_name = 'Portal';
        if(!$reply_to_email) 
         $reply_to_email = $from_email;
        if(!$reply_to_name) 
         $reply_to_name = $from_name;
        if(!$subject) 
         $subject = 'This is a test';
		 
	//echo 'From Name: '.$from_name.'<br /> From Email: '.$from_email.' <br>Reply To: '.$reply_to_name."<br> Subject: ".$subject.'<br>Body: '.$message.'<br>Post Subject: '.$post_subject."<br> Attachment: ".$attachment."<br> Use Bcc: ".$use_bcc;	 exit;
       $subject.=$post_subject;
	   $this->load->library('email');
       $this->email->initialize($config);
		
       $this->email->from($from_email, $from_name);
       $this->email->reply_to($reply_to_email, $reply_to_name);
       if($use_bcc)
	   {
	     $this->email->to(array($from_email));	
	     $this->email->bcc($send_to_list); 
	   }
	   else
   	     $this->email->to($send_to_list);	

	   $this->email->subject($subject);
	
	   $this->email->message($message);
	   if($attachment)
       	 $this->email->attach($attachment);
       if($this->email->send())
	   {
         return true;
       }
	   else
	    return false;
	
	  // echo $this->email->print_debugger();
	}
   function fileUpload($path='',$file_control_name='',$custome_config=array())
   {
        $config['upload_path'] = ($path) ? $path : './images/';
		$config['allowed_types'] = '*';
		//$config['max_size']	= '100';
		//$config['max_width']  = '1024';
		//$config['max_height']  = '768';
		if($config && is_array($config) && count($config))
		{
		 $config=array_merge($config,$custome_config);
		}
        $return->error=true;
		$return->message='';
		$this->load->library('upload', $config);
  		if (!$this->upload->do_upload($file_control_name))
		{
			$return->message=$this->upload->display_errors();
		}
		else
		{
		    $return->error=false;
			$return->message=$this->upload->data();
		}
		return $return;
   }
    function removeFiles($files=array())
	{
	  foreach($files as $file)
	  {
	    if(file_exists($file))
	      unlink($file);
	  }
	}
	
	
	public function check_password($user_id, $password)
	{
		if (empty($password))
			return FALSE;

		// Hash the password
		if (is_string($password))
		{
			$user = $this->user->find(array('userid'=>$user_id));

			// Get the salt from the stored password
			$stored_pass=$user['password'];
			$salt = $this->find_salt($stored_pass);
			// Create a hashed password using the salt from the stored password
			$password = $this->hash_password($password, $salt);
			
			return ($password==$stored_pass) ? true : false;
		 }
		 else
		   return false;
	}
	
	public function hash_password($password, $salt = FALSE)
	{
	    $this->config->load('auth');
		if(!is_array($this->config->item('salt_pattern')))
		{
		  $salt_pattern = preg_split('/,\s*/', $this->config->item('salt_pattern'));
		  $this->config->set_item('salt_pattern', $salt_pattern);
		}
		if ($salt === FALSE)
		{
			// Create a salt seed, same length as the number of offsets in the pattern
			$salt = substr($this->hash(uniqid(NULL, TRUE)), 0, count($this->config->item('salt_pattern')));
		}
		// Password hash that the salt will be inserted into
		$hash = $this->hash($salt.$password);
		// Change salt to an array
		$salt = str_split($salt, 1);
		// Returned password
		$password = '';
		// Used to calculate the length of splits
		$last_offset = 0;
    	foreach ($this->config->item('salt_pattern') as $offset)
		{
			// Split a new part of the hash off
			$part = substr($hash, 0, $offset - $last_offset);
			// Cut the current part out of the hash
			$hash = substr($hash, $offset - $last_offset);
			// Add the part to the password, appending the salt character
			$password.= $part.array_shift($salt);
			// Set the last offset to the current offset
			$last_offset = $offset;
		}
		// Return the password, with the remaining hash appended
		return $password.$hash;
	}

	/**
	 * Perform a hash, using the configured method.
	 *
	 * @param   string  string to hash
	 * @return  string
	 */
	public function hash($str)
	{
	  $this->config->load('auth');
      return hash($this->config->item('hash_method'), $str);
	}

	/**
	 * Finds the salt from a password, based on the configured salt pattern.
	 *
	 * @param   string  hashed password
	 * @return  string
	 */
	public function find_salt($password)
	{
        $this->config->load('auth');
		if(!is_array($this->config->item('salt_pattern')))
		{
		  $salt_pattern = preg_split('/,\s*/', $this->config->item('salt_pattern'));
		  $this->config->set_item('salt_pattern', $salt_pattern);
		}
		$salt = '';
		foreach ($this->config->item('salt_pattern') as $i => $offset)
		{
			// Find salt characters, take a good long look...
			$salt .= substr($password, $offset + $i, 1);
		}

		return $salt;
	}
	function font_array($more_fonts=array())
	{
	     $fonts = array(
		'Arial,Arial,Helvetica,sans-serif',
		'Arial Black,Arial Black,Gadget,sans-serif',
		'Comic Sans MS,Comic Sans MS,cursive',
		'Courier New,Courier New,Courier,monospace',
		'Georgia,Georgia,serif',
		'Impact,Charcoal,sans-serif',
		'Lucida Console,Monaco,monospace',
		'Lucida Sans Unicode,Lucida Grande,sans-serif',
		'Palatino Linotype,Book Antiqua,Palatino,serif',
		'Tahoma,Geneva,sans-serif',
		'Times New Roman,Times,serif',
		'Trebuchet MS,Helvetica,sans-serif',
		'Verdana,Geneva,sans-serif' );
		if(is_array($more_fonts) && count($more_fonts))
		{
		   $fonts=array_merge($fonts,$more_fonts);
		}
		return $fonts;
	}
    function get_font_options($selected_font='',$more_fonts=array())
	{
	  $fonts=$this->font_array($more_fonts);
	  return makeoptions($fonts,'value','',$selected_font);
	}
	function xmlToArray($contents )
	{
	   $contents = str_replace(array("\n", "\r", "\t"), '', $contents);
	   $contents = trim(str_replace('"', "'", $contents));
	   $contents = @simplexml_load_string($contents);
	   $contents = json_encode($contents);
	   $contents = json_decode($contents);
	   return $contents ;
	}
   function curlPost($url=NULL,$params=array(),$api_headers=array(),$api_key=NULL,$secret_key=NULL,$post_fields=array(),$get_fields=array())
   {
        $baseURL=$url;
        $APIKEY_HEADER ="App-Authorization";
        $SIGNATURE_HEADER = "App-Signature";
		
		$args=array();
        if($api_key) { $args['apikey']=$api_key; }
		
		if(is_array($get_fields) && count($get_fields))
		{
		  $args=array_merge($args,$get_fields);
		}
		
		$argstring = "";
		foreach($args as $k => $v)
		{
		  if(!$argstring)
            $argstring = sprintf("%s=%s",$k,$v);
          else
            $argstring .= sprintf("&%s=%s",$k,$v);            
        }
		$param='';
		if(is_array($params) && count($params))
		 $param= implode('/',$params);
		
		$headers=array();
		if(is_array($api_headers) && count($api_headers))
		{
		  foreach($api_headers as $k => $v)
		  {
		    $headers[]=$k.':'.urlencode(trim($v));   
		  }
		}
		
		if($api_key)
		 $headers[]=$APIKEY_HEADER.':'.urlencode(trim($api_key));
		if($secret_key)
		{
			//$signature = base64_encode(hash_hmac('sha256', utf8_encode($url_path), utf8_encode($secret_key)));
			//$headers[]=$SIGNATURE_HEADER.':'.urlencode(trim($signature));
			$signature = base64_encode(hash_hmac('sha256', utf8_encode(trim($url_path)), utf8_encode(trim($secret_key)), true));
            $headers[]=$SIGNATURE_HEADER.':'.$signature;
    	}
		
		$return->status=false;
		$return->result="";
		if($url)
		{
		    $user_agent='Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';
			$ch = curl_init ();
			curl_setopt($ch, CURLOPT_URL, $url);
			$whitelist = array('localhost', '127.0.0.1');
			if(in_array($_SERVER['HTTP_HOST'], $whitelist)){
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			}
            curl_setopt($ch, CURLOPT_USERAGENT,"$user_agent");
			//curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_AUTOREFERER, true);
            if($headers && count($headers))
			 curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
			curl_setopt( $ch, CURLOPT_FAILONERROR,1);
         //curl_setopt($ch, CURLOPT_NOBODY ,1);
		    if($headers && count($headers))
			{
			  //  curl_setopt($ch,CURLOPT_POST,count($post_fields));	
				curl_setopt ($ch, CURLOPT_POST, 1);
				curl_setopt($ch,CURLOPT_POSTFIELDS, $post_fields);//"push_type=1&message=".urlencode($message));
			}
			//curl_setopt($ch, CURLOPT_USERPWD,"$user:$pass");
		    curl_setopt($ch, CURLOPT_TIMEOUT, 100); // times out after 10s
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
			// make the request and store the result
			$result = curl_exec ($ch);
			if($result === false)
			{
				$return->result=curl_error($ch);
			}
			else
			{
				$return->status=true;
				$return->result=$result;
			}
			// parse the result (json)
			//json_decode($result);
		}
		return $return;     
   }
   function html2rgb($color)
{

    if ($color[0] == '#')
        $color = substr($color, 1);
    if (strlen($color) == 6)
        list($r, $g, $b) = array($color[0].$color[1],
                                 $color[2].$color[3],
                                 $color[4].$color[5]);
    elseif (strlen($color) == 3)
        list($r, $g, $b) = array($color[0].$color[0], $color[1].$color[1], $color[2].$color[2]);
    else
        return false;
    $r = hexdec($r); $g = hexdec($g); $b = hexdec($b);
    return array($r, $g, $b);
}
function rgb2html($r, $g=-1, $b=-1)
{
    if (is_array($r) && sizeof($r) == 3)
       list($r, $g, $b) = $r;

    $r = intval($r); $g = intval($g);
    $b = intval($b);

    $r = dechex($r<0?0:($r>255?255:$r));
    $g = dechex($g<0?0:($g>255?255:$g));
    $b = dechex($b<0?0:($b>255?255:$b));

    $color = (strlen($r) < 2?'0':'').$r;
	$color .= (strlen($g) < 2 ? '0' : '').$g;
    $color .= (strlen($b) < 2 ? '0' : '').$b;
    return '#'.$color;
}


   function isMobileBrowser()
   {
      $useragent=$_SERVER['HTTP_USER_AGENT'];
if(preg_match('/android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|meego.+mobile|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) return 'mobile';
   }
   function ftp_mod_change($file_or_folders="",$mode=0664)
   {
     $return=false;
     if(!$file_or_folders)
	  return "";
	 $mode = intval($mode, 8); 
	 $ftp="ftp.testftp.com";
	 $ftp_user="";
	 $ftp_pass="";
	 $ftp_root="";
     $conn = @ftp_connect($ftp);// or die("Could not connect");
     ftp_login($conn,$ftp_user,$ftp_pass);
	 if(is_array($file_or_folders) && count($file_or_folders))
	 {
	   foreach($file_or_folders as $path)
	   {
	     //ftp_chmod($conn,$mode,$path);
		 if (ftp_site($conn, 'CHMOD '.$mode.' '.$ftp_root.$path) !== false)
		   $return=TRUE;
	   }
	 }
	 else
	 {
	    $path=file_or_folders;
	    ftp_chmod($conn,$mode,$path);
	    if (ftp_site($conn, 'CHMOD '.$mode.' '.$ftp_root.$path) !== false)
		   $return=TRUE;
	 }
	 ftp_close($conn);
	 return  $return;
   }
   function lang_list()
   {
	   $langs=array(''=>'Language','en'=>'EN','de'=>'DE','fr'=>'FR','nl'=>'NL');
	   return $langs; 
   }
   function get_lang_text($value="en")
   {
	   $langs=$this->lang_list();
	   $lang_text= (isset($langs[$value])) ? $langs[$value] : '';
	   return $lang_text; 
   }
   function lang_options()
   {
	   $current_lang=$this->session->get_userdata("site_lang");
	   $current_lang=($current_lang) ? $current_lang : 'en';
	   $lang_list=$this->lang_list();
	   return makeoptions($lang_list,'','',$current_lang);
	   ///$this->session->set_userdata('lang' , 'ca');
   }
}
?>