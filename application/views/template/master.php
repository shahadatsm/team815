<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title><?php echo $title; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/'); ?>" media="screen" />
	
	
</head>

<body>
	<div id="header">
		<?php echo $header; ?>
	</div>
	
	<div id="center">
		<?php 
		
			if(isset($body))
			{
				echo '<br>';
				echo '<div align="center">'; 
				echo $body;
				echo '</div>';
			}
			elseif(isset($message))
			{
				//do nothing
			}
			else 
			{
				echo '<br><br><br><br><br><br><br><br><br><br><br>';
				echo '<center><h1>This page is your playground.</h1></center>';
			} 
		
		?>
	</div>
	
	<div id="footer">
		<?php echo $footer; ?>
		
		<br><br>
		<center>Copyright 2015</center>
	</div>
</body>
</html>