<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
 ?> 
   <?php  if($this->authentication->is_someone_loggedin()) {  ?>	
 <!-- left menu starts -->

        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
				
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu">
                        <li class="nav-header">Main</li>
                        <li><a class="ajax-link" href="<?php echo config_item("base_url");?>"><i class="glyphicon glyphicon-home"></i><span> Dashboard</span></a>
                        </li>
                        <li class="accordion">
                            <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Manage User</span></a>
                            <ul class="nav nav-pills nav-stacked">
                                <li><?php echo secure_anchor('createusers/list_user/7','Administrators'); ?></li>
                                <li><?php echo secure_anchor('createusers/list_user/6','Representative'); ?></li>
								<li><?php echo secure_anchor('createusers/list_user/5','Non Representative'); ?></li>
								<li><?php echo secure_anchor('createusers/list_user/3','Clients'); ?></li>
						    </ul> 
                        </li>
						<li class="accordion">
                            <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Manage Article</span></a>
                            <ul class="nav nav-pills nav-stacked">
                                <li><?php echo secure_anchor('product_article/','Article Listing'); ?></li>
                            </ul>
                        </li>
						<li class="accordion">
                            <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Manage Catalogs</span></a>
                            <ul class="nav nav-pills nav-stacked">
                                <li><?php echo secure_anchor('catalogmaker/createcatalog','Catalog Listing'); ?></li>
                            </ul>
                        </li>
                       
                    </ul>
                
				</div>
            </div>
        </div>
        <!--/span-->
        <!-- left menu ends -->
	  <?php } ?>	