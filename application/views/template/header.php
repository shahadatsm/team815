<!DOCTYPE HTML>
<html <?php echo ( defined('offline_cache')) ? 'manifest="portal.appcache"' : '' ?> >
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
   <meta name="apple-mobile-web-app-capable" content="yes">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" /> 
   <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" /> 
   <?php if(isset($apple_touch_icon) && $apple_touch_icon) { ?>
   <link href="<?php echo $apple_touch_icon;?>" rel="apple-touch-icon" />
   <?php } ?>
   <link type="image/x-icon" rel="shortcut icon" href="<?php echo config_item('base_url');?>favicon.ico">
   <title><?php echo isset($title) ? config_item('site_name').' : '.$title : config_item('site_name')?></title>
  
    <?php 
      $query_version="1.10.2.min"; 
   ?>
    <link rel="stylesheet" href="<?php echo config_item('base_url');?>css/style.css">
	<link rel="stylesheet" href="<?php echo config_item('base_url');?>css/yogesh.css">
	
	
	    <!-- The styles -->
    <link id="bs-css" href="<?php echo config_item('base_url');?>script-style/charisma/css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="<?php echo config_item('base_url');?>script-style/charisma/css/charisma-app.css" rel="stylesheet">
    <link href='<?php echo config_item('base_url');?>script-style/charisma/bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/css/jquery.noty.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/css/noty_theme_default.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/css/elfinder.min.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/css/elfinder.theme.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/css/uploadify.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/css/animate.min.css' rel='stylesheet'>

    <!-- jQuery -->
    <script src="<?php echo config_item('base_url');?>script-style/charisma/bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon 
	
	
	<link id="bs-css" href="<?php echo config_item('base_url');?>script-style/charisma/css/bootstrap-cerulean.min.css" rel="stylesheet">
	  
	<link href="<?php echo config_item('base_url');?>script-style/charisma/css/charisma-app.css" rel="stylesheet">
    <link href='<?php echo config_item('base_url');?>script-style/charisma/bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/css/jquery.noty.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/css/noty_theme_default.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/css/elfinder.min.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/css/elfinder.theme.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/css/uploadify.css' rel='stylesheet'>
    <link href='<?php echo config_item('base_url');?>script-style/charisma/css/animate.min.css' rel='stylesheet'>
	<link rel="stylesheet" href="<?php echo config_item('base_url');?>script-style/bootstrap/bootstrap.min.css"> 
	 <script src="<?php echo config_item('base_url');?>script-style/charisma/bower_components/jquery/jquery.min.js"></script>
	-->

	
   

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

	
 <!-- <script src="<?php echo config_item('base_url');?>script-style/jquery/1.11.3/jquery.min.js"></script> -->
 <!-- <script src="<?php echo config_item('base_url');?>script-style/bootstrap/bootstrap.min.js"></script> -->
  <script type="text/javascript" src="<?php echo config_item('base_url');?>js/validator.min.js"> </script>

   <script type="text/javascript">var base_url = '<?php echo config_item('base_url');?>';   </script>
 
  <!-- <script src="<?php echo config_item('base_url');?>js/jquery/<?php echo $query_version?>/jquery.js"></script> -->  

  

  
 <?php
     if(isset($script_prior_to_jqm)) echo $script_prior_to_jqm;
 ?> 
 <?php
     if(isset($extraheader)) echo $extraheader;
 ?>
<script language="javascript" type="text/javascript">jQuery(document).ready(function() {
  <?php echo isset($onloadextraheader) ? $onloadextraheader : '';?>
});

$(window).bind("load", function() {

  <?php echo isset($onwindowload) ? $onwindowload: '';?>
    
});

function set_lang(value)
{
	 if(!value) return;
	 var url=base_url+"welcome/switch_language/";
		$.post(url,
		{language:value},
		  function(response)
		  {
			  if(response.status)
			  {
				window.location.reload();
			  }
	
		},'json');
}

</script>

<?php

if(ENVIRONMENT!='development')
{
 $catalog_ua_code=(isset($google_ua_code) && $google_ua_code) ? $google_ua_code : "";
 if($catalog_ua_code)
 {
	 $ua_code="<script type='text/javascript'> var _gaq = _gaq || []; _gaq.push(['_setAccount', '".$catalog_ua_code."']); _gaq.push(['_trackPageview']);";
	 if(isset($track_event) && $track_event)
	   $ua_code.=$track_event;  
	 $ua_code.=" (function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })(); </script>";
	 echo(isset($ua_code) && $ua_code) ? $ua_code : ''; 
 }
}
?>
</head> 
<body>

<img src="<?php echo base_url()?>images/loading.gif" style="display:none;">

 <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html"> <img style="display:none;" alt="Charisma Logo" src="img/logo20.png" class="hidden-xs"/>
                <span>Catelog</span></a>
	<?php  if($this->authentication->is_someone_loggedin()) { $user_data=$this->authentication->check_login(9); ?>
            <!-- user dropdown starts -->
            <div  class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> <?php echo (isset($user_data->user_name)) ? $user_data->user_name : '' ?></span>
                    <span class="caret"></span>
                </button>
		
                <ul class="dropdown-menu">
                    <li><a href="#">Profile</a></li>
                    <li class="divider"></li>
                    <li>
	                   <?php echo secure_anchor('logout','Logout'); ?> 
					</li>
                </ul>
            </div>
            <!-- user dropdown ends -->
 <?php } ?>
 
 <!-- theme selector starts -->
            <div class="btn-group pull-right theme-container animated tada">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-tint"></i><span
                        class="hidden-sm hidden-xs"><?php echo (isset($site_lang_text)) ? $site_lang_text : '' ?></span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" id="langs">
                    <li><a onclick="set_lang('en');" href="#"><i class="whitespace"></i> EN</a></li>
                    <li><a  onclick="set_lang('de');" href="#"><i class="whitespace"></i> DE</a></li>
                    <li><a onclick="set_lang('fr');"  href="#"><i class="whitespace"></i> FR</a></li>
                    <li><a onclick="set_lang('nl');"  href="#"><i class="whitespace"></i> NL</a></li>
                   </ul>
            </div>
			
  <!--        
  <ul style="display:none;" class="collapse navbar-collapse nav navbar-nav top-menu">
                <li><a href="#"><i class="glyphicon glyphicon-globe"></i> Visit Site</a></li>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-star"></i> Dropdown <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>
                <li>
                    <form class="navbar-search pull-left">
                        <input placeholder="Search" class="search-query form-control col-md-10" name="query"
                               type="text">
                    </form>
                </li>
            </ul>
-->
		  

          
        </div>
    </div>
    <!-- topbar ends -->
	


<div class="ch-container">
   <div class="row">