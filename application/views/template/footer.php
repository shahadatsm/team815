</div>
<hr />
 <footer class="row">
        <p class="col-md-9 col-sm-9 col-xs-12 copyright">&copy; <a href="http://usman.it" target="_blank">Catalog</a> 2012 - 2015</p>

        <p class="col-md-3 col-sm-3 col-xs-12 powered-by">Powered by: <a
                href="#">Catalog</a></p>
    </footer>

</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="<?php echo config_item('base_url');?>script-style/charisma/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="<?php echo config_item('base_url');?>script-style/charisma/js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='<?php echo config_item('base_url');?>script-style/charisma/bower_components/moment/min/moment.min.js'></script>
<script src='<?php echo config_item('base_url');?>script-style/charisma/bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='<?php echo config_item('base_url');?>script-style/charisma/js/jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="<?php echo config_item('base_url');?>script-style/charisma/bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="<?php echo config_item('base_url');?>script-style/charisma/bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="<?php echo config_item('base_url');?>script-style/charisma/js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="<?php echo config_item('base_url');?>script-style/charisma/bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="<?php echo config_item('base_url');?>script-style/charisma/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="<?php echo config_item('base_url');?>script-style/charisma/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="<?php echo config_item('base_url');?>script-style/charisma/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="<?php echo config_item('base_url');?>script-style/charisma/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="<?php echo config_item('base_url');?>script-style/charisma/js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="<?php echo config_item('base_url');?>script-style/charisma/js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="<?php echo config_item('base_url');?>script-style/charisma/js/charisma.js"></script>

<script  type="text/javascript">var projectBase = "<?php echo base_url();?>";</script>
<script type="text/javascript" src="<?php echo config_item('base_url');?>js/custom-scripts.js"> </script>

</body>


</html>
