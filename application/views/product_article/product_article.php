<div id="content" class="col-lg-10 col-sm-10">
    <fieldset>
        <legend>Create Article</legend>
        <form id="loginForm" data-toggle="validator"  class="form-horizontal" action="<?php echo (isset($action_link)) ? $action_link : '' ?>" method="post" role="form">
            <input type="hidden" name="user_level" id="user_level" value="<?php echo (isset($user_level)) ? $user_level : '' ?>" />
            <div class="row" style="text-align:center;"><?php echo (isset($message)) ? $message : '' ?></div> 
            <div class="form-group">
                <label for="article_number" class="control-label col-sm-2" >Article Number</label>
                <div class="input-group col-sm-10">
				   <input type="hidden" name="product_article_id" id="product_article_id" value="<?php echo set_value('product_article_id', ((isset($product_article_id) && $product_article_id) ? $product_article_id : '')) ?>" />
                    <input  type="text" class="form-control" id="article_number" name="article_number" placeholder="Article Number" value="<?php echo set_value('article_number', ((isset($article_number) && $article_number) ? $article_number : '')) ?>"  /><?php echo form_error('article_number') ?> 
                </div>
            </div>
            <div class="form-group">
                <label for="ean_code" class="control-label col-sm-2" >Ean Code</label>
                <div class="input-group col-sm-10">
                    <input  type="text" class="form-control" id="ean_code" name="ean_code" placeholder="Ean Code" value="<?php echo set_value('ean_code', ((isset($ean_code) && $ean_code) ? $ean_code : '')) ?>"  /><?php echo form_error('ean_code') ?> 
                </div>
            </div>
            <div class="form-group">
                <label for="article_brand" class="control-label col-sm-2" >Article Brand</label>
                <div class="input-group col-sm-10">
                    <input  type="text" class="form-control" id="article_brand" name="article_brand" placeholder="Article Brand" value="<?php echo set_value('article_brand', ((isset($article_brand) && $article_brand) ? $article_brand : '')) ?>"  /><?php echo form_error('article_brand') ?> 
                </div>
            </div>
            <div class="form-group">
                <label for="material_type" class="control-label col-sm-2" >Material Type</label>
                <div class="input-group col-sm-10">
                    <input  type="text" class="form-control" id="material_type" name="material_type" placeholder="Material Type" value="<?php echo set_value('material_type', ((isset($material_type) && $material_type) ? $material_type : '')) ?>" required /><?php echo form_error('material_type') ?> 
                </div>
            </div>
            <div class="form-group">
                <label for="extra_info" class="control-label col-sm-2" >Extra Info</label>
                <div class="input-group col-sm-10">
                    <input  type="text" class="form-control" id="extra_info" name="extra_info" placeholder="Extra Info" value="<?php echo set_value('extra_info', ((isset($extra_info) && $extra_info) ? $extra_info : '')) ?>" required /><?php echo form_error('extra_info') ?> 
                </div>
            </div>
            <div class="form-group">
                <label for="product_name" class="control-label col-sm-2" >Product Name</label>
                <div class="input-group col-sm-10">
                    <input  type="text" class="form-control" id="product_name" name="product_name" placeholder="Product Name" value="<?php echo set_value('product_name', ((isset($product_name) && $product_name) ? $product_name : '')) ?>" required /><?php echo form_error('product_name') ?> 
                </div>
            </div>
            <div class="form-group">
                <label for="weight" class="control-label col-sm-2" >Weight</label>
                <div class="input-group col-sm-10">
                    <input  type="text" class="form-control" id="weight" name="weight" placeholder="Weight" value="<?php echo set_value('weight', ((isset($weight) && $weight) ? $weight : '')) ?>"  /><?php echo form_error('weight') ?> 
                </div>
            </div>
            <div class="form-group">
                <label for="width" class="control-label col-sm-2" >Width</label>
                <div class="input-group col-sm-10">
                    <input  type="text" class="form-control" id="width" name="width" placeholder="Width" value="<?php echo set_value('width', ((isset($width) && $width) ? $width : '')) ?>"  /><?php echo form_error('width') ?> 
                </div>
            </div>
            <div class="form-group">
                <label for="length" class="control-label col-sm-2" >Length</label>
                <div class="input-group col-sm-10">
                    <input  type="text" class="form-control" id="length" name="length" placeholder="Length" value="<?php echo set_value('length', ((isset($length) && $length) ? $length : '')) ?>"  /><?php echo form_error('length') ?> 
                </div>
            </div>
            <div class="form-group">
                <label for="article_group" class="control-label col-sm-2" >Article Group</label>
                <div class="input-group col-sm-10">
                    <input  type="text" class="form-control" id="article_group" name="article_group" placeholder="Article Group" value="<?php echo set_value('article_group', ((isset($article_group) && $article_group) ? $article_group : '')) ?>"  /><?php echo form_error('article_group') ?> 
                </div>
            </div>
            <div class="form-group">
                <label for="article_description" class="control-label col-sm-2" >Article Description</label>
                <div class="input-group col-sm-10">
                    <input  type="text" class="form-control" id="article_description" name="article_description" placeholder="Article Description" value="<?php echo set_value('article_description', ((isset($article_description) && $article_description) ? $article_description : '')) ?>"  /><?php echo form_error('article_description') ?> 
                </div>
            </div>
            <div class="form-group">
                <label for="design_name" class="control-label col-sm-2" >Design Name</label>
                <div class="input-group col-sm-10">
                    <input  type="text" class="form-control" id="design_name" name="design_name" placeholder="Design Name" value="<?php echo set_value('design_name', ((isset($design_name) && $design_name) ? $design_name : '')) ?>"  /><?php echo form_error('design_name') ?> 
                </div>
            </div>
            <div class="form-group">
                <label for="colors" class="control-label col-sm-2" >Color</label>
                <div class="input-group col-sm-10">
                    <textarea class="form-control" id="colors" name="colors" placeholder="Color"><?php echo set_value('colors', ((isset($colors) && $colors) ? $colors : '')) ?></textarea> 
                    <?php echo form_error('colors') ?> 
                </div>
            </div>
            <div class="form-group">
                <label for="unity" class="control-label col-sm-2" >Unity</label>
                <div class="input-group col-sm-10">
                    <textarea class="form-control" id="unity" name="unity" placeholder="Unity"><?php echo set_value('unity', ((isset($unity) && $unity) ? $unity : '')) ?></textarea> 
                    <?php echo form_error('unity') ?> 
                </div>
            </div>
            <div class="form-group">
                <label for="selling_unity" class="control-label col-sm-2" >Selling Unity</label>
                <div class="input-group col-sm-10">
                    <textarea class="form-control" id="selling_unity" name="selling_unity" placeholder="Selling Unity"><?php echo set_value('selling_unity', ((isset($selling_unity) && $selling_unity) ? $selling_unity : '')) ?></textarea> 
                    <?php echo form_error('selling_unity') ?> 
                </div>
            </div>
            <div class="form-group">
                <label for="packing_unity" class="control-label col-sm-2" >Packing Unity</label>
                <div class="input-group col-sm-10">
                    <textarea class="form-control" id="packing_unity" name="packing_unity" placeholder="Packing Unity"><?php echo set_value('Packing Unity', ((isset($packing_unity) && $packing_unity) ? $packing_unity : '')) ?></textarea> 
                    <?php echo form_error('Packing Unity') ?> 
                </div>
            </div>
            <div class="form-group">
                <label for="price" class="control-label col-sm-2" >Price</label>
                <div class="input-group col-sm-10">
                    <textarea class="form-control" id="price" name="price" placeholder="price"><?php echo set_value('price', ((isset($price) && $price) ? $price : '')) ?></textarea> 
                    <?php echo form_error('price') ?> 
                </div>
            </div>
            <div class="form-group">
                <label for="status" class="control-label col-sm-2" >Status</label>
                <div class="input-group col-sm-10">
                    <textarea class="form-control" id="status" name="status" placeholder="status"><?php echo set_value('status', ((isset($status) && $status) ? $status : '')) ?></textarea> 
                    <?php echo form_error('status') ?> 
                </div>
            </div>
            <div class="form-group">

                <div class="col-xs-offset-2 col-xs-10">

                    <button type="submit" class="btn btn-primary">Save</button>  <a href="<?php echo config_item("base_url").$this->router->fetch_class()?>/"  class="btn btn-default">Back</a>

                </div>

            </div>

        </form>
    </fieldset>
</div>

<script>
	$(document).ready(function() {
	   
	   //$('#loginForm').validator()
	});
</script>
