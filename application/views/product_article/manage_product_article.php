<div id="content" class="col-lg-10 col-sm-10">
		<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Manage Product Article</a>
					</li>
					<li>
						<a href="#">List Product Article</a>
					</li>
				</ul>
		</div>
		<div class="">
		<?php 
		      $href="";
			  $anchor="";
		 
	  $href="product_article/add_article";
			  $anchor="Create Product Article";
				?>
		<a class="btn btn-primary" href="<?php echo config_item("base_url").$href?>"><?php echo $anchor ?></a> </div>
		
		<table class="table">
		 <thead>
		  <tr>
		   <th>Article Number</th>
		   <th>Ean Code</th>
		   <th>Article Brand</th>
		   <th>Article Group</th>
		   <th>Material Type</th>
		   <th>Extra Info</th>
		   <th>Product Name</th>
		   <th>Weight</th>
		   <th>Length</th>
		   
		   <th>Article Description</th>
		   <th>Design Name</th>
		   <th>Colors</th>
		   <th>Unity</th>
		   <th>Selling Unity</th>
		   <th>Packing Unity</th>
		   <th>Price</th>
		   <th>Length</th>
		   <th></th>
		   
		  </tr> 
		 </thead>
		 <tbody>
		   <?php
		      $content="";
  		      if(is_array($articles) && count($articles)){ 
		         foreach($articles as $article) {
					$content.='<tr>'; 
					$content.='<td>'.$article['article_number'].'</td>';
					$content.='<td>'.$article['ean_code'].'</td>';
					$content.='<td>'.$article['article_brand'].'</td>';
					$content.='<td>'.$article['article_group'].'</td>';
					$content.='<td>'.$article['material_type'].'</td>';
					$content.='<td>'.$article['extra_info'].'</td>';
					$content.='<td>'.$article['product_name'].'</td>';
					$content.='<td>'.$article['weight'].'</td>';
					$content.='<td>'.$article['width'].'</td>';
					$content.='<td>'.$article['length'].'</td>';
					$content.='<td>'.$article['article_description'].'</td>';
					$content.='<td>'.$article['design_name'].'</td>';
					$content.='<td>'.$article['colors'].'</td>';
					$content.='<td>'.$article['unity'].'</td>';
					$content.='<td>'.$article['selling_unity'].'</td>';
					$content.='<td>'.$article['packing_unity'].'</td>';
					$content.='<td>'.$article['price'].'</td>';
					$content.='<td>'.$article['length'].'</td>';
					$content.='<td><a href="'.config_item('base_url').$this->router->fetch_class().'/edit_article/'.$article['product_article_id'].'" >edit</a> </td>';
					$content.='</tr>';
		   ?>
			   
			   
		   <?php } } echo $content; ?>
		 </tbody>
		 
		</table>
		
		
		
 </div><!--/#content.col-md-0-->