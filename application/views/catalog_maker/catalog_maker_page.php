<fieldset>
  <legend>CATALOG MAKER</legend>  
    <div class="row">
      <div class="col-sm-12 catalog-maker-page-show-msg text-center"></div>
    </div>
    <br />

  
    <div class="form-group">
        <label for="main_catalog_lang" class="control-label text-right col-sm-6" >Main Catalog Language</label>
        <div class="input-group col-sm-6">
            <select class="form-control" id="main_catalog_lang" name="main_catalog_lang" />
                <option value="">Select Main Catalog Language</option>
                <option value="en">EN</option>
                <option value="fr">FR</option>
                <option value="de">DE</option>
                <option value="nl">NL</option>
            </select>
        </div>
    </div>
    <br /><br />
  
        
    <div class="row">
        <div class="col-sm-6">
              <input  type="text" class="form-control" id="article_id" name="article_id" placeholder="Article ID" value=""  />    
        </div>    
        <div class="col-sm-6">
              <button class="btn btn-md btn-block btn-primary get-article-details">GET ARTCILE DETAILS</button>
        </div>    
    </div>
    <br />
  
  <table class="table table-striped" id="articlelist">
  		<thead>
        	<th>Article number</th>
            <th>Brand</th>
            <th>Group</th>
            <th>Language</th>
        </thead>
        <tbody>        	
        </tbody>
  </table>
    
  
</fieldset>