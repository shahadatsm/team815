<div id="content" class="col-lg-10 col-sm-10">
		<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Manage User</a>
					</li>
					<li>
						<a href="#">List User</a>
					</li>
				</ul>
		</div>
		<div class="">
		<?php 
		      $href="";
			  $anchor="";
		      switch($user_level){
			  case 7:
				$href="createusers/administrators";
				$anchor="Create Administrators";
			  break;
			   case 6:
				$href="createusers/representatives";
				$anchor="Create Representatives";
			  break;
			   case 5:
				$href="createusers/nonrepresentatives";
				$anchor="Create Non-Representatives";
			  break;
			   case 3:
				$href="createusers/clients";
				$anchor="Create Clients";
			  break;
			  default:
			  break;
		} ?>
		<a class="btn btn-primary" href="<?php echo config_item("base_url").$href?>"><?php echo $anchor ?></a> </div>
		
		<table class="table">
		 <thead>
		  <tr>
		   <th>User Name</th>
		   <th>First Name</th>
		   <th>Last Name</th>
		   <th>Telephone</th>
		   <th>Mobile</th>
		   <th>Address</th>
		   <th>City</th>
		   <th>Postal Code</th>
		   <th>State</th>
		  </tr> 
		 </thead>
		 <tbody>
		   <?php
		      $content="";
  		      if(is_array($users) && count($users)){ 
		         foreach($users as $user) {
					$content.='<tr>'; 
					$content.='<td>'.$user['user_name'].'</td>';
					$content.='<td>'.$user['first_name'].'</td>';
					$content.='<td>'.$user['last_name'].'</td>';
					$content.='<td>'.$user['tel'].'</td>';
					$content.='<td>'.$user['mobile'].'</td>';
					$content.='<td>'.$user['address'].'</td>';
					$content.='<td>'.$user['city'].'</td>';
					$content.='<td>'.$user['postal_code'].'</td>';
					$content.='<td>'.$user['state'].'</td>';
					$content.='</tr>';
		   ?>
			   
			   
		   <?php } } echo $content; ?>
		 </tbody>
		 
		</table>
		
		
		
 </div><!--/#content.col-md-0-->