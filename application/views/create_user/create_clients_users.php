<div id="content" class="col-lg-10 col-sm-10">
<fieldset>
  <legend>CREATE CLIENTS</legend>
<form id="loginForm" data-toggle="validator"  class="form-horizontal" action="<?php echo (isset($action_link)) ? $action_link : ''?>" method="post" role="form">
        <input type="hidden" name="user_level" id="user_level" value="<?php echo (isset($user_level)) ?  $user_level : '' ?>" />
        <div class="row" style="text-align:center;"><?php echo (isset($message)) ? $message : '' ?></div> 
		<div class="form-group">
			<label for="owned_company" class="control-label col-sm-2" >Owned Company</label>
			<div class="input-group col-sm-10">
				<select class="form-control" id="owned_company" name="owned_company" />
                	<option value="">Select Owned Company</option>
                    <option value="Owned Company Name 1">Owned Company Name 1</option>
                    <option value="Owned Company Name 2">Owned Company Name 2</option>
                    <option value="Owned Company Name 3">Owned Company Name 3</option>
                </select>
                <?php echo form_error('owned_company');?> 
			</div>
		</div>
	   <div class="form-group">
			<label for="last_name" class="control-label col-sm-2" >Default Language</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="default_language" name="default_language" placeholder="Default Language(EN, FR, DE, NL)" value="<?php echo set_value('default_language',((isset($default_language) && $default_language) ? $default_language : ''))?>"  /><?php echo form_error('default_language');?> 
			</div>
		</div>
	     <div class="form-group">
			<label for="purchase_central" class="control-label col-sm-2" >Purchase Central</label>
			<div class="input-group col-sm-10">
				<select class="form-control" id="purchase_central" name="purchase_central" />
                	<option value="">Select Purchase Central</option>
                    <option value="Purchase Central 1">Purchase Central 1</option>
                    <option value="Purchase Central 2">Purchase Central 2</option>
                    <option value="Purchase Central 3">Purchase Central 3</option>
                </select>
                <?php echo form_error('purchase_central');?> 
			</div>
		</div>
	     <div class="form-group">
			<label for="price_central" class="control-label col-sm-2" >Price Central</label>
			<div class="input-group col-sm-10">
				<select class="form-control" id="price_central" name="price_central" />
                	<option value="">Select Price Central</option>
                    <option value="Price Central 1">Price Central 1</option>
                    <option value="Price Central 2">Price Central 2</option>
                    <option value="Price Central 3">Price Central 3</option>
                </select>
                <?php echo form_error('price_central');?> 
			</div>
		</div>        
	     <div class="form-group">
			<label for="client_type" class="control-label col-sm-2" >Type Of Client</label>
			<div class="input-group col-sm-10">
				<select class="form-control" id="client_type" name="client_type" />
                	<option value="">Select Type Of Client</option>
                    <option value="Type Of Client 1">Type Of Client 1</option>
                    <option value="Type Of Client 2">Type Of Client 2</option>
                    <option value="Type Of Client 3">Type Of Client 3</option>
                </select>
                <?php echo form_error('client_type');?> 
			</div>
		</div>        
	     <div class="form-group">
			<label for="shop_type" class="control-label col-sm-2" >Type Of Shop</label>
			<div class="input-group col-sm-10">
				<select class="form-control" id="shop_type" name="shop_type" />
                	<option value="">Select Type Of Shop</option>
                    <option value="Type Of Shop 1">Type Of Shop 1</option>
                    <option value="Type Of Shop 2">Type Of Shop 2</option>
                    <option value="Type Of Shop 3">Type Of Shop 3</option>
                </select>
                <?php echo form_error('shop_type');?> 
			</div>
		</div>        
		 <div class="form-group">
			<label for="client_short_name" class="control-label col-sm-2" >Client Short Name</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="client_short_name" name="client_short_name" placeholder="Client Short Name" value="<?php echo set_value('client_short_name',((isset($client_short_name) && $client_short_name) ? $client_short_name : ''))?>" required /><?php echo form_error('client_short_name');?> 
			</div>
		</div>
		 <div class="form-group">
			<label for="verification_code" class="control-label col-sm-2" >Verification Code</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="verification_code" name="verification_code" placeholder="Verification Code" value="<?php echo set_value('verification_code',((isset($verification_code) && $verification_code) ? $verification_code : ''))?>" required /><?php echo form_error('verification_code');?> 
			</div>
		</div>
		 <div class="form-group">
			<label for="company_name" class="control-label col-sm-2" >Company Name</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="company_name" name="company_name" placeholder="Company Name" value="<?php echo set_value('company_name',((isset($company_name) && $company_name) ? $company_name : ''))?>" required /><?php echo form_error('company_name');?> 
			</div>
		</div>        
		 <div class="form-group">
			<label for="vat_id" class="control-label col-sm-2" >VAT ID</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="vat_id" name="vat_id" placeholder="vat_id" value="<?php echo set_value('vat_id',((isset($vat_id) && $vat_id) ? $vat_id : ''))?>" required /><?php echo form_error('vat_id')?> 
			</div>
		</div>                
		 <div class="form-group">
			<label for="user_name" class="control-label col-sm-2" >User Name</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="user_name" name="user_name" placeholder="User Name" value="<?php echo set_value('user_name',((isset($user_name) && $user_name) ? $user_name : ''))?>" required /><?php echo form_error('user_name');?> 
			</div>
		</div>        
		 <div class="form-group">
			<label for="user_pass" class="control-label col-sm-2" >User Password</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="user_pass" name="user_pass" placeholder="User Password" value="<?php echo set_value('user_pass',((isset($user_pass) && $user_pass) ? $user_pass : ''))?>" required /><?php echo form_error('user_pass');?> 
			</div>
		</div>        
		 <div class="form-group">
			<label for="passconf" class="control-label col-sm-2" >Confirm Password</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="passconf" name="passconf" placeholder="Confirm Password" value="<?php echo set_value('passconf',((isset($passconf) && $passconf) ? $passconf : ''))?>" required /><?php echo form_error('passconf');?> 
			</div>
		</div>
	     <div class="form-group">
			<label for="user_email" class="control-label col-sm-2" >Email ID</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="user_email" name="user_email" placeholder="User Email" value="<?php echo set_value('user_email',((isset($user_email) && $user_email) ? $user_email : ''))?>"  /><?php echo form_error('user_email');?> 
			</div>
		</div>        
		<div class="form-group">
			<label for="tel" class="control-label col-sm-2" >Tel</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="tel" name="tel" placeholder="Tel" value="<?php echo set_value('tel',((isset($tel) && $tel) ? $tel : ''))?>"  /><?php echo form_error('tel');?> 
			</div>
		</div>
		<div class="form-group">
			<label for="mobile" class="control-label col-sm-2" >Mobile</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile" value="<?php echo set_value('mobile',((isset($mobile) && $mobile) ? $mobile : ''))?>"  /><?php echo form_error('mobile');?> 
			</div>
		</div>
		<div class="form-group">
			<label for="address" class="control-label col-sm-2" >Address</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="address" name="address" placeholder="Address" value="<?php echo set_value('address',((isset($address) && $address) ? $address : ''))?>"  /><?php echo form_error('address');?> 
			</div>
		</div>
		<div class="form-group">
			<label for="city" class="control-label col-sm-2" >City</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="city" name="city" placeholder="City" value="<?php echo set_value('city',((isset($city) && $city) ? $city : ''))?>"  /><?php echo form_error('city');?> 
			</div>
		</div>
		<div class="form-group">
			<label for="postal_code" class="control-label col-sm-2" >Zip/Postal Code</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="postal_code" name="postal_code" placeholder="Zip/Postal Code" value="<?php echo set_value('postal_code',((isset($postal_code) && $postal_code) ? $postal_code : ''))?>"  /><?php echo form_error('postal_code');?> 
			</div>
		</div>
		<div class="form-group">
			<label for="state_province" class="control-label col-sm-2" >State / Prov.</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="state_province" name="state_province" placeholder="State / Prov." value="<?php echo set_value('state_province',((isset($state_province) && $state_province) ? $state_province : ''))?>"  /><?php echo form_error('state_province');?> 
			</div>
		</div>
		<div class="form-group">
			<label for="contact_details" class="control-label col-sm-2" >Contact details</label>
			<div class="input-group col-sm-10">
				<textarea class="form-control" id="contact_details" name="contact_details" placeholder="Contact details"><?php echo set_value('contact_details',((isset($contact_details) && $contact_details) ? $contact_details : ''))?></textarea> 
				<?php echo form_error('contact_details');?> 
			</div>
		</div>
		<div class="form-group">
			<label for="notes" class="control-label col-sm-2" >Notes</label>
			<div class="input-group col-sm-10">
				<textarea class="form-control" id="notes" name="notes" placeholder="Notes"><?php echo set_value('notes',((isset($notes) && $notes) ? $notes : ''))?></textarea> 
				<?php echo form_error('notes');?> 
			</div>
		</div>
        <div class="form-group">

            <div class="col-xs-offset-2 col-xs-10">

                <button type="submit" class="btn btn-primary">Save</button>

            </div>

        </div>

</form>
</fieldset>
      </div>