<div id="content" class="col-lg-10 col-sm-10">
<fieldset>
  <legend>CREATE REPRESENTAIVE</legend>
<form id="loginForm" data-toggle="validator"  class="form-horizontal" action="<?php echo (isset($action_link)) ? $action_link : ''?>" method="post" role="form">
        <input type="hidden" name="user_level" id="user_level" value="<?php echo (isset($user_level)) ?  $user_level : '' ?>" />
        <div class="row" style="text-align:center;"><?php echo (isset($message)) ? $message : '' ?></div> 
		<div class="form-group">
			<label for="first_name" class="control-label col-sm-2" >First Name</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="<?php echo set_value('first_name',((isset($first_name) && $first_name) ? $first_name : ''))?>"  /><?php echo form_error('first_name')?> 
			</div>
		</div>
	   <div class="form-group">
			<label for="last_name" class="control-label col-sm-2" >Last Name</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="<?php echo set_value('last_name',((isset($last_name) && $last_name) ? $last_name : ''))?>"  /><?php echo form_error('last_name')?> 
			</div>
		</div>
	     <div class="form-group">
			<label for="user_email" class="control-label col-sm-2" >User Email</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="user_email" name="user_email" placeholder="User Email" value="<?php echo set_value('user_email',((isset($user_email) && $user_email) ? $user_email : ''))?>"  /><?php echo form_error('user_email')?> 
			</div>
		</div>
		 <div class="form-group">
			<label for="user_name" class="control-label col-sm-2" >User Name</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="user_name" name="user_name" placeholder="User Name" value="<?php echo set_value('user_name',((isset($user_name) && $user_name) ? $user_name : ''))?>" required /><?php echo form_error('user_name')?> 
			</div>
		</div>
		 <div class="form-group">
			<label for="user_pass" class="control-label col-sm-2" >User Password</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="user_pass" name="user_pass" placeholder="User Password" value="<?php echo set_value('user_pass',((isset($user_pass) && $user_pass) ? $user_pass : ''))?>" required /><?php echo form_error('user_pass')?> 
			</div>
		</div>
		 <div class="form-group">
			<label for="passconf" class="control-label col-sm-2" >Confirm Password</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="passconf" name="passconf" placeholder="Confirm Password" value="<?php echo set_value('passconf',((isset($passconf) && $passconf) ? $passconf : ''))?>" required /><?php echo form_error('passconf')?> 
			</div>
		</div>
		<div class="form-group">
			<label for="tel" class="control-label col-sm-2" >Tel</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="tel" name="tel" placeholder="Tel" value="<?php echo set_value('tel',((isset($tel) && $tel) ? $tel : ''))?>"  /><?php echo form_error('tel')?> 
			</div>
		</div>
		<div class="form-group">
			<label for="mobile" class="control-label col-sm-2" >Mobile</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile" value="<?php echo set_value('mobile',((isset($mobile) && $mobile) ? $mobile : ''))?>"  /><?php echo form_error('mobile')?> 
			</div>
		</div>
		<div class="form-group">
			<label for="address" class="control-label col-sm-2" >Address</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="address" name="address" placeholder="Address" value="<?php echo set_value('address',((isset($address) && $address) ? $address : ''))?>"  /><?php echo form_error('address')?> 
			</div>
		</div>
		<div class="form-group">
			<label for="city" class="control-label col-sm-2" >City</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="city" name="city" placeholder="City" value="<?php echo set_value('city',((isset($city) && $city) ? $city : ''))?>"  /><?php echo form_error('city')?> 
			</div>
		</div>
		<div class="form-group">
			<label for="postal_code" class="control-label col-sm-2" >Zip/Postal Code</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="postal_code" name="postal_code" placeholder="Zip/Postal Code" value="<?php echo set_value('postal_code',((isset($postal_code) && $postal_code) ? $postal_code : ''))?>"  /><?php echo form_error('postal_code')?> 
			</div>
		</div>
		<div class="form-group">
			<label for="state_province" class="control-label col-sm-2" >State / Prov.</label>
			<div class="input-group col-sm-10">
				<input  type="text" class="form-control" id="state_province" name="state_province" placeholder="State / Prov." value="<?php echo set_value('state_province',((isset($state_province) && $state_province) ? $state_province : ''))?>"  /><?php echo form_error('state_province')?> 
			</div>
		</div>
		<div class="form-group">
			<label for="contact_details" class="control-label col-sm-2" >Contact details</label>
			<div class="input-group col-sm-10">
				<textarea class="form-control" id="contact_details" name="contact_details" placeholder="Contact details"><?php echo set_value('contact_details',((isset($contact_details) && $contact_details) ? $contact_details : ''))?></textarea> 
				<?php echo form_error('contact_details')?> 
			</div>
		</div>
		<div class="form-group">
			<label for="notes" class="control-label col-sm-2" >Notes</label>
			<div class="input-group col-sm-10">
				<textarea class="form-control" id="notes" name="notes" placeholder="Notes"><?php echo set_value('notes',((isset($notes) && $notes) ? $notes : ''))?></textarea> 
				<?php echo form_error('notes')?> 
			</div>
		</div>
        <div class="form-group">

            <div class="col-xs-offset-2 col-xs-10">

                <button type="submit" class="btn btn-primary">Save</button>

            </div>

        </div>

</form>
</fieldset>
</div>