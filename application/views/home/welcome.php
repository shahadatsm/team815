<script>
$(document).ready(function() {
   
   //$('#loginForm').validator()
});
</script>
<div class="container">
  <h2>Horizontal form</h2>
 <form id="loginForm" data-toggle="validator"  class="form-horizontal" action="<?php echo (isset($action_link)) ? $action_link : ''?>" method="post" role="form">
	<div class="form-group">
		<label class="control-label col-sm-2" for="inputName">Name</label>
		<div class="input-group col-sm-10">
			<input id="inputName" class="form-control " type="text" required="" placeholder="Cina Saffary">
		</div>
	</div>
	<div class="form-group">
		<label for="inputName" class="control-label col-sm-2" >Name</label>
		<div class="input-group col-sm-10">
			<input  type="user_name" class="form-control" id="user_name" name="user_name" placeholder="Enter User Name" value="<?php echo set_value('user_name',((isset($user_name) && $user_name) ? $user_name : ''))?>" required /><?php echo form_error('user_name')?> 
		</div>
	</div>
  <div class="form-group has-feedback">
    <label for="inputTwitter" class="control-label col-sm-2">Twitter</label>
    <div class="input-group col-sm-10">
      <span class="input-group-addon">@</span>
      <input type="text" pattern="^[_A-z0-9]{1,}$" maxlength="15" class="form-control" id="inputTwitter" placeholder="1000hz" required>
    </div>
    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
  </div>
  <div class="form-group">
    <label for="inputEmail" class="control-label col-sm-2">Email</label>
     <div class="input-group col-sm-10">
	   <input type="email" class="form-control" id="inputEmail" placeholder="Email" data-error="Bruh, that email address is invalid" required>
    </div>
	<div class="help-block with-errors"></div>
  </div>
  <div class="form-group">
    <label for="password" class="control-label col-sm-2">Password</label>
    <div class="input-group col-sm-10">    
	  <input type="password" data-minlength="6" class="form-control" id="inputPassword" placeholder="Password" required>
 	  <span class="help-block">Minimum of 6 characters</span>
    </div>    
  </div>
 <div class="form-group">
    <label for="password" class="control-label col-sm-2">Confirm Password</label>
    <div class="input-group col-sm-10">     
	<input type="password" class="form-control" id="inputPasswordConfirm" data-match="#inputPassword" data-match-error="Whoops, these don't match" placeholder="Confirm" required>
      <span class="help-block">Minimum of 6 characters</span>
	</div>  
  </div>  
  
  
  <div class="form-group">
     <div class="col-sm-offset-2 col-sm-10"> 
    <div class="radio">
      <label>
        <input type="radio" name="underwear" required>
        Boxers
      </label>
    </div>
    <div class="radio">
      <label>
        <input type="radio" name="underwear" required>
        Briefs
      </label>
    </div>
	
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10"> 
    <div class="checkbox">
      <label>
        <input type="checkbox" id="terms" data-error="Before you wreck yourself" required>
        Check yourself
      </label>
      <div class="help-block with-errors"></div>
    </div>
	</div>
  </div>
  <div class="form-group">
     <div class="col-sm-offset-2 col-sm-10">
       <button type="submit" class="btn btn-primary">Submit</button>
	  </div> 
  </div>
</form>

</div>
