<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Community Auth - MY Model
 *
 * Community Auth is an open source authentication application for CodeIgniter 3
 *
 * @package     Community Auth
 * @author      Robert B Gottier
 * @copyright   Copyright (c) 2011 - 2015, Robert B Gottier. (http://brianswebdesign.com/)
 * @license     BSD - http://www.opensource.org/licenses/BSD-3-Clause
 * @link        http://community-auth.com
 */

class MY_Model extends CI_Model {
    
		private $READ;
    private $WRITE;

	/**
	 * Value of the primary key ID of the record that this model is currently pointing to
	 *
	 * @var unknown_type
	 * @access public
	 */
	var $id = null;

	/**
	 * Container for the data that this model gets from persistent storage (the database).
	 *
	 * @var array
	 * @access public
	 */
	var $data = array();

/**
	 * Container for the data to use in where condition of a query that this model gets from persistent storage (the database).
	 *
	 * @var array
	 * @access public
	 */
	var $where = array();


	/**
 	 * The name of the associate table name of the Model object
 	 * @var string
 	 * @access public
 	 */
	var $_table;

	/**
	 * The name of the ID field for this Model.
	 *
	 * @var string
	 * @access public
	 */
	var $primaryKey = 'id';

	/**
	 * Container for the fields of the table that this model gets from persistent storage (the database).
	 *
	 * @var array
	 * @access public
	 */
	var $fields = array();

	/**
	 * The last inserted ID of the data that this model created
	 *
	 * @var int
	 * @access private
	 */
	var $__insertID = null;

	/**
	 * The number of records returned by the last query
	 *
	 * @access private
	 * @var int
	 */
	var $__numRows = null;

	/**
	 * The number of records affected by the last query
	 *
	 * @access private
	 * @var int
	 */
	var $__affectedRows = null;

	/**
	 * Tells the model whether to return results in array or not
	 *
	 * @var string
	 * @access public
	 */
	var $returnArray = TRUE;

	/**
	 * Prints helpful debug messages if asked
	 *
	 * @var string
	 * @access public
	 */
	var $debug = FALSE;
	
	/**
	 * An array specifying the form validation error delimeters.
	 * They can be conveniently set in either the controller or model.
	 * I like to use a list for my errors, and CI default is for 
	 * individual paragraphs, which I think is somewhat retarded.
	 *
	 * @var array
	 * @access public
	 */
	public $error_delimiters = array( '<li>', '</li>' );

	/**
	 * An array specifying which fields to unset from 
	 * the form validation class' protected error array.
	 * This is helpful if you have hidden fields that 
	 * are required, but the user shouldn't see them 
	 * if form validation fails.
	 *
	 * @var string
	 * @access public
	 */
	public $hide_errors = array();

	/**
	 * All form validation errors are stored as a string, 
	 * and can be accessed from the controller or model.
	 *
	 * @var string
	 * @access public
	 */
	public $validation_errors   = '';

	/**
	 * Validation rules are set in the model, since 
	 * the model is aware of what data should be inserted 
	 * or updated. The exception would be when using the 
	 * reauthentication feature, because we can optionally 
	 * pass in our validation rules from the controller.
	 *
	 * @var string
	 * @access public
	 */
	public $validation_rules = array();
	
	// --------------------------------------------------------------

	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}

	// --------------------------------------------------------------

	/**
	 * Form validation consolidation.
	 */
	public function validate()
	{
		// Load the form validation library
		$this->load->library('form_validation');

		// Apply the form validation error delimiters
		$this->_set_form_validation_error_delimiters();

		// Set form validation rules
		$this->form_validation->set_rules( $this->validation_rules );

		// If form validation passes
		if( $this->form_validation->run() !== FALSE )
		{
			// Load var to confirm validation passed
			$this->load->vars( array( 'validation_passed' => 1 ) );

			return TRUE;
		}

		/**
		 * If form validation passes, none of the code below will be processed.
		 */

		// Unset fields from the error array if they are in the hide errors array.
		if( ! empty( $this->hide_errors ) )
		{
			foreach( $this->hide_errors as $field )
			{
				$this->form_validation->unset_error( $field );
			}
		}

		// Load errors into class member for use in model or controller.
		$this->validation_errors = validation_errors();

		// Load var w/ validation errors
		$this->load->vars( array( 'validation_errors' => $this->validation_errors ) );

		/**
		 * Do not repopulate with data that did not validate
		 */

		// Get the errors
		$error_array = $this->form_validation->get_error_array();

		// Loop through the post array
		foreach( $this->input->post() as $k => $v )
		{
			// If a key is in the error array
			if( array_key_exists( $k, $error_array ))
			{
				// kill set_value() for that key
				$this->form_validation->unset_field_data( $k );
			}
		}

		return FALSE;
	}

	// --------------------------------------------------------------

	/**
	 * Sometimes, when you have a successful form validation, 
	 * you will not want to repopulate the form, but if you 
	 * don't unset the field data, the form will repopulate.
	 */
	public function kill_set_value()
	{
		$this->form_validation->unset_field_data('*');
	}

	// --------------------------------------------------------------

	/**
	 * Set the form validation error delimiters with an array.
	 */
	private function _set_form_validation_error_delimiters()
	{
		list( $prefix, $suffix ) = $this->error_delimiters;

		$this->form_validation->set_error_delimiters( $prefix, $suffix );
	}

	// --------------------------------------------------------------
	
		/**
	 * Load the associated database table.* @access public
	 */

	function loadTable($table, $fields = array())
	{
		if ($this->debug) log_message('debug', "Loading model table: $table");

		$this->_table  = $table;
		$this->fields = (!empty($fields)) ? $fields : $this->db->list_fields($table);

		if ($this->debug)
		{
			log_message('debug', "Successfully Loaded model table: $table");
		}
	}

	/**
	 * Returns a resultset array with specified fields from database matching given conditions.* @return query result either in array or in object based on model config
	 * @access public
	 */

	function findAll($conditions = NULL,$orConditions=NULL, $fields = '*', $order = NULL, $start = 0, $limit = NULL,$joinTable=NULL,$joinConditions=NULL)
	{
	    if($joinTable && $joinConditions)
		{
		  $this->db->join($joinTable, $joinConditions);
		}
		if ($conditions != NULL)
		{
			if(is_array($conditions))
			{
				$this->db->where($conditions);
			}
			else
			{
				$this->db->where($conditions, NULL, FALSE);
			}
			if($orConditions)			
			  $this->db->or_where($orConditions);
		}

		if ($fields != NULL)
		{
			$this->db->select($fields);
		}

		if ($order != NULL)
		{
			$this->db->order_by($order);
		}

		if ($limit != NULL)
		{
			$this->db->limit($limit, $start);
		}

		$query = $this->db->get($this->_table);
		$this->__numRows = $query->num_rows();

		return ($this->returnArray) ? $query->result_array() : $query->result();
	}

	/**
	 * Return a single row as a resultset array with specified fields from database matching given conditions.* @return single row either in array or in object based on model config
	 * @access public
	 */

	function find($conditions = NULL, $fields = '*', $order = NULL,$joinTable=NULL,$joinConditions=NULL)
	{
		$data = $this->findAll($conditions,NULL, $fields, $order, 0, 1,$joinTable,$joinConditions);

		if ($data)
		{
			return $data[0];
		}
		else
		{
			return false;
		}
	}
     /*
	   * Returns contents of a field in a query matching given conditions.* @return string the value of the field specified of the first row
	   * @access public
     */

	function field($conditions = null, $name, $fields = '*', $order = NULL,$joinTable=NULL,$joinConditions=NULL)
	{
		$data = $this->findAll($conditions,NULL, $fields, $order, 0, 1,$joinTable,$joinConditions);
//echo '<pre>';print_r($data ); echo $name;exit;
		if ($data)
		{
			$row = $data[0];

			if (isset($row[$name]))
			{
				return $row[$name];
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}

	}

	/**
	 * Returns number of rows matching given SQL condition.* @return integer the number of records returned by the condition
	 * @access public
	 */

	function findCount($conditions = null,$orConditions=NULL)
	{
		$data = $this->findAll($conditions,$orConditions,'COUNT(*) AS count', null, 0, 1);

		if ($data)
		{
			return $data[0]['count'];
		}
		else
		{
			return false;
		}
	}

	/**
	 * Returns a key value pair array from database matching given conditions.
	 *
	 * Example use: generateList(null, '', 0. 10, 'id', 'username');
	 * Returns: array('10' => 'abc', '11' => 'xyz')* @return array a list of key val ue pairs given criteria
	 * @access public
	 */

	function generateList($conditions = null, $order = '', $start = 0, $limit = NULL, $key = null, $value = null)
	{
		$data = $this->findAll($conditions, NULL, "$key, $value", $order, $start, $limit);

		if ($data)
		{
			foreach ($data as $row)
			{
				$keys[] = ($this->returnArray) ? $row[$key] : $row->$key;
				$vals[] = ($this->returnArray) ? $row[$value] : $row->$value;
			}

			if (!empty($keys) && !empty($vals))
			{
				$return = array_combine($keys, $vals);
				return $return;
			}
		}
		else
		{
			return false;
		}
	}

	/**
	 * Returns an array of the values of a specific column from database matching given conditions.
	 *
	 * Example use: generateSingleArray(null, 'name');* @return array a list of key value pairs given criteria
	 * @access public
	 */

	function generateSingleArray($conditions = null, $field = null, $order = 'id ASC', $start = 0, $limit = NULL)
	{
		$data = $this->findAll($conditions, NULL, "$field", $order, $start, $limit);

		if ($data)
		{
		
			foreach ($data as $row)
			{
				$arr[] = ($this->returnArray) ? $row[$field] : $row->$field;
			}

			return $arr;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Initializes the model for writing a new record.* @return boolean True
	 * @access public
	 */

	function create()
	{
		$this->id = false;
		unset ($this->data);
        unset ($this->where);
		$this->data = array();
		$this->where = array();
		return true;
	}

	/**
	 * Returns a list of fields from the database and saves in the model* @return array Array of database fields
	 * @access public
	 */

	function read($id = null, $fields = null)
	{
		if ($id != null)
		{
			$this->id = $id;
		}

		$id = $this->id;

		if ($this->id !== null && $this->id !== false)
		{
			$this->data = $this->find($this->primaryKey . ' = ' . $id, $fields);
			return $this->data;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Inserts a new record in the database.* @return boolean success
	 * @access public
	 */

	function insert($data = null)
	{
		if ($data == null)
		{
		    if(!$this->data)
			  return FALSE;
		}
		else
		 $this->data = $data;
		$this->data['create_date'] = date("Y-m-d H:i:s");
		foreach ($this->data as $key => $value)
		{
			if (array_search($key, $this->fields) === FALSE)
			{
				unset($this->data[$key]);
			}
		}

		$this->db->insert($this->_table, $this->data);

		$this->__insertID = $this->db->insert_id();
		return $this->__insertID;
	}
	
	function insert_batch($data = null)
	{
		if ($data == null)
		{
		    if(!$this->data)
			  return FALSE;
		}
		else
		 $this->data = $data;
	//print_r($this->data);	 
		//$this->data['create_date'] = date("Y-m-d H:i:s");
		foreach ($this->data as $d)
		{
		    foreach($d as $key => $value)
			{
				if (array_search($key, $this->fields) === FALSE)
				{
					unset($d[$key]);
				}
		   }	  	
		}
		$this->db->insert_batch($this->_table, $this->data);

		$this->__insertID = $this->db->insert_id();
		return $this->__insertID;
	}
	
	/**
	 * Update record(s) in the database.* @return boolean success
	 * @access public
	 */

	function update($data = null,$where=NULL)
	{
	    if ($data == null)
		{
		    if(!$this->data)
			  return FALSE;
		}
		else
		 $this->data = $data;
		
		if ($where == null)
		{
		    if(!$this->where)
			  return FALSE;
		}
		else
		 $this->where = $where;
		
	  
		$this->data['last_update'] = date("Y-m-d H:i:s");

		foreach ($this->data as $key => $value)
		{
			if (array_search($key, $this->fields) === FALSE)
			{
				unset($this->data[$key]);
			}
		}
        return $this->db->update($this->_table, $this->data,$this->where);
	}

	/**
	 * Saves model data to the database.* @return boolean success
	 * @access public
	 */

	function save($data = null, $id = null)
	{
		if ($data)
		{
			$this->data = $data;
		}

		foreach ($this->data as $key => $value)
		{
			if (array_search($key, $this->fields) === FALSE)
			{
				unset($this->data[$key]);
			}
		}

		if ($id != null)
		{
			$this->id = $id;
		}

		$id = $this->id;

		if ($this->id !== null && $this->id !== false)
		{
			$this->db->where($this->primaryKey, $id);
			$this->db->update($this->_table, $this->data);

			$this->__affectedRows = $this->db->affected_rows();
			return $this->id;
		}
		else
		{
			$this->db->insert($this->_table, $this->data);

			$this->__insertID = $this->db->insert_id();
			return $this->__insertID;
		}
	}

	/**
	 * Removes record for given id. If no id is given, the current id is used. Returns true on success.
	 * @return boolean True on success
	 * @access public
	 */

	function remove($id = null)
	{
		if ($id != null)
		{
			$this->id = $id;
		}

		$id = $this->id;

		if (($this->id !== null && $this->id !== false) && ($this->primaryKey !== null && $this->primaryKey !== false))
		{
			if ($this->db->delete($this->_table, array($this->primaryKey => $id)))
			{
				$this->id = null;
				$this->data = array();

				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
		   if(count($this->where))
		   {
		        if ($this->db->delete($this->_table, $this->where))
				{
					$this->id = null;
					$this->data = array();
                 	$this->where = array();
					return true;
				}
				else
				{
					return false;
				}
		   }
		   else
		   	return false;
		}
	}

	/**
	 * Returns a resultset for given SQL statement. Generic SQL queries should be made with this method.* @return array Resultset
	 * @access public
	 */

	function query($sql)
	{
		return $this->db->query($sql);
	}
	
	function getResult($sql=NULL,$where=NULL)
	{
	    if($sql)
		{
	      $query = $this->query($sql);
		}
		else
		{
		  if($where)
		  {
		    $query = $this->query("SELECT * FROM ".$this->_table.$where);
		  }
		} 
		if( $query->num_rows())
		{
		 $this->__numRows = $query->num_rows();
         return ($this->returnArray) ? $query->result_array() : $query->result();
		}
		else
		 return false; 
	}

	/**
	 * Returns the last query that was run (the query string, not the result).* @return string SQL statement
	 * @access public
	 */

	function lastQuery()
	{
		return $this->db->last_query();
	}

	/**
	 * This function simplifies the process of writing database inserts. It returns a correctly formatted SQL insert string.* @return string SQL statement
	 * @access public
	 */

	function insertString($data)
	{
		return $this->db->insert_string($this->_table, $data);
	}

	/**
	 * Returns the current record's ID.* @return integer The ID of the current record
	 * @access public
	 */

	function getID()
	{
		return $this->id;
	}

	/**
	 * Returns the ID of the last record this Model inserted.* @return int
	 * @access public
	 */

	function getInsertID()
	{
		return $this->__insertID;
	}

	/**
	 * Returns the number of rows returned from the last query.* @return int
	 * @access public
	 */

	function getNumRows()
	{
		return $this->__numRows;
	}

	/**
	 * Returns the number of rows affected by the last query* @return int
	 * @access public
	 */

	function getAffectedRows()
	{
		return $this->__affectedRows;
	}
	

	/* backup the db OR just a table */
	function backup_tables($tables = '*',$type="full",$write_path='',$operation='wd')
	{
	 
	  $return="";
	  //get all of the tables
	  if($tables == '*')
	  {
		$tables = array();
		$query = $this->db->query('SHOW TABLES');
		if($query->num_rows())
		{
			foreach ($query->result_array() as $row) {
			  foreach($row as $k=>$v)
			  {
				$tables[] = $v;
			  }	 
			} 
		}
	  }
	  else
	  {
		$tables = is_array($tables) ? $tables : explode(',',$tables);
	  }
	  //cycle through
	  foreach($tables as $table)
	  {
		$query = $this->db->query('SELECT * FROM '.$table);
		$num_fields = $query->num_fields();
		$results=$query->result_array();
		
		if($type=='full' || $type=='schema')
		{
		    $return.= 'DROP TABLE  IF EXISTS '.$table.';';
			$query2=$this->db->query('SHOW CREATE TABLE '.$table);
			if($query2->num_rows())
			{
			  $row2 = $query2->row_array();
			  array_shift($row2);
			  foreach($row2 as $r2=>$v2)
			  {
				$return.= "\n\n".$v2.";\n\n";
			  }	
			}
		}
		
		if($type=='full' || $type=='data')
		{
		
		  foreach($results as $row)
		  {
			$row = array_values($row);
			
			$return.= 'INSERT INTO '.$table.' VALUES(';
			for($j=0; $j<$num_fields; $j++) 
			{
			  if($row[$j])
			  {
				  $row[$j] = addslashes($row[$j]);
				  $row[$j] = preg_replace("/\n/","\\n",$row[$j]);
				  if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
				  if ($j<($num_fields-1)) { $return.= ','; }
			  }	  
			}
			 $return.= ");\n";
		   }
		   $return.="\n\n\n";
	     }
	  }
	  //save file
	  //if(isset(config_item("bak_path")) && config_item("bak_path"))
	  //{
	//   if(!is_dir(config_item("bak_path")))
	//     mkdir(config_item("bak_path"),0700);
	//  }
	  $file_ext='.sql';
	  $file_name='db-backup-'.$type.'-'.time().'-'.(md5(implode(',',$tables)));
	  
	  $this->load->library('zip');
	  $this->zip->add_data($file_name.$file_ext, $return);
		// Write the zip file to a folder on your server. Name it "my_backup.zip"
	  $file_path=($write_path) ? $write_path : config_item("bak_path");
	  if($operation=='wd' || $operation=='w')
	    $this->zip->archive($file_path.$file_name.'.zip'); 
      if($operation=='wd' || $operation=='d')
  	    $this->zip->download($file_name.'.zip'); 
/*
	  
	  $handle = fopen($file_name,'w+');
	  fwrite($handle,$return);
	  fclose($handle);
	  */
	}
	
	 function update_order($where=array(), $field='', $new_order=NULL,$id=NULL,$id_field=NULL){
	
		//Escape for security
		if(!$field || $new_order==NULL) return false;
		$prev=0;
		if($id)
		{
		  if($id_field)
		  {
		    $condition[$id_field]=$id;
		    $prev=$this->field($condition,$field);
		    $prev =($prev) ? $prev : 0; 
			
		   }	
		}
	
		if($prev > 0)
		{
			//Shift all other entries back into old (abandoned) position
			$this->where=$where;
			$this->where[$field.' > ']=$prev;
			$this->db->set($field, $field.' - 1', FALSE);
			$this->db->where($this->where);
			$this->db->update($this->_table);
		}

		//Shift all entries forward to insert into new position
		$this->where=$where;
		$this->where[$field.' >= ']=$new_order;
		$this->db->set($field, $field.' + 1', FALSE);
		$this->db->where($this->where);
    	$this->db->update($this->_table);
//print_r($this->lastQuery()); exit;
	}
}

/* End of file MY_Model.php */
/* Location: /application/libraries/MY_Model.php */