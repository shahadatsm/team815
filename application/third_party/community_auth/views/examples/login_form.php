<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Community Auth - Login Form View
 *
 * Community Auth is an open source authentication application for CodeIgniter 3
 *
 * @package     Community Auth
 * @author      Robert B Gottier
 * @copyright   Copyright (c) 2011 - 2015, Robert B Gottier. (http://brianswebdesign.com/)
 * @license     BSD - http://www.opensource.org/licenses/BSD-3-Clause
 * @link        http://community-auth.com
 */
 ?>
  <div id="content" class="col-lg-10 col-sm-10 lf">
 <?php           

if( ! isset( $on_hold_message ) )
{
	if( isset( $login_error_mesg ) )
	{
		echo '
			<div style="border:1px solid red;">
				<p>
					Login Error: Invalid Username, Email Address, or Password.
				</p>
				<p>
					Username, email address and password are all case sensitive.
				</p>
			</div>
		';
	}

	if( $this->input->get('logout') )
	{
		echo '
			<div style="border:1px solid green">
				<p>
					You have successfully logged out.
				</p>
			</div>
		';
	}

	echo form_open( $login_url, array( 'class' => 'std-form' ) ); 
?>



<div class="row form-horizontal">
 <div class="well col-md-5 center login-box">
	<div class="alert alert-info"> 
		
		<?php  if( ! isset( $optional_login ) )
		{
			echo 'LOGIN TO YOUR ACCOUNT';
		} ?>
    </div>
 <form id="loginForm" class="" data-toggle="validator" action="<?php echo (isset($action_link)) ? $action_link : ''?>" method="post" role="form">
<fieldset>
  <div class="input-group input-group-lg">
	<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
	<input  type="text" autocomplete="off" maxlength="255"  class="form-control" id="login_string" name="login_string" placeholder="Username" value="<?php echo set_value('login_string',((isset($login_string) && $login_string) ? $login_string : ''))?>" required /><?php echo form_error('login_string')?> 
   </div>
  
  
  <div class="input-group input-group-lg pass_data">
    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
	 <input  type="password" autocomplete="off" maxlength="<?php echo config_item('max_chars_for_password'); ?>"  class="form-control" id="login_pass" name="login_pass" placeholder="Password" value="<?php echo set_value('login_pass',((isset($login_pass) && $login_pass) ? $login_pass : ''))?>" required /> 
 
  </div>
  
  
 
  
  
 
  
    <p class="center login_data">
    
       <input type="submit" class="btn btn-primary signin" name="submit" value="Sign In" id="submit_button"  />
	 
	</p>
 
   
   <div class="input-prepend">
     
	 
	 <label class="remember" for="remember"><input type="checkbox" id="remember_me" name="remember_me"  value="yes" data-error="Before you wreck yourself"> Remember me</label>
    <?php
			if( config_item('allow_remember_me') )
			{
		?>
	<?php
			}
		?>
	
	 <a href="<?php echo secure_site_url('examples/recover'); ?>" class="forgot_pass">Forgot Password?</a>
  </div>
  

  </fieldset> 
	
</form>
</div>
<?php echo form_error('login_pass')?>
 </div>
<?php

	}
	else
	{
		// EXCESSIVE LOGIN ATTEMPTS ERROR MESSAGE
		echo '
			<div style="border:1px solid red;">
				<p>
					Excessive Login Attempts
				</p>
				<p>
					You have exceeded the maximum number of failed login<br />
					attempts that this website will allow.
				<p>
				<p>
					Your access to login and account recovery has been blocked for ' . ( (int) config_item('seconds_on_hold') / 60 ) . ' minutes.
				</p>
				<p>
					Please use the ' . secure_anchor('examples/recover','Account Recovery') . ' after ' . ( (int) config_item('seconds_on_hold') / 60 ) . ' minutes has passed,<br />
					or contact us if you require assistance gaining access to your account.
				</p>
			</div>
		';
	}

/* End of file login_form.php */
/* Location: /views/examples/login_form.php */ 
?>

</div>