<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Createusers extends CI_Controller {

	
	
    public function __construct()
    {
        parent::__construct();
		$this->load->model("user_model");	
        $this->load->model("user_details_model");		
		$checkUserLogin = $this->authentication->check_login(9);
		if ( ! isset($checkUserLogin->user_level) ){ redirect('/login/', 'refresh');}
		
        // Force SSL
        //$this->force_ssl();		
    }
	
	
	
	
	public function index()
	{			
        $data =getCommonData(); 
		$loggedUser = $this->authentication->user_status(9);
		$data['auth_user_id'] = isset($loggedUser->user_id)?$loggedUser->user_id : '';
		$data['auth_user_level'] = isset($loggedUser->user_level)?$loggedUser->user_level : '';
		$data['auth_user_name'] = isset($loggedUser->user_name)?$loggedUser->user_name : '';
		
	
		
		//$output = $this->load->view('create_user/create_administrators_users' , $data , true);
		$output = $this->load->view('create_user/admin_home' , $data , true);
		//print_r($output);
		output($output , $data);
		
	}
	public function create_administrators_users($user_level = 7)
	{
		$data = getCommonData(); 
		$data['title'] = 'User Entry';
		$data['user_level']=$user_level;
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('first_name', 'First Name', 'max_length[12]|trim|required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'max_length[12]|trim|required');
		$this->form_validation->set_rules('user_name', 'User Name', 'callback_username_check|max_length[12]|alpha_numeric|required');
		$this->form_validation->set_rules('user_pass', 'Password', 'trim|required|matches[passconf]|external_callbacks[model,formval_callbacks,_check_password_strength,TRUE]');
		$this->form_validation->set_rules('passconf', 'Confirm Password', 'trim|required|external_callbacks[model,formval_callbacks,_check_password_strength,TRUE]');
		$this->form_validation->set_rules('user_email', 'Email', 'callback_email_check|required|valid_email');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span>'); 
		$data['action_link']= current_url();
		
		if ($this->form_validation->run() == FALSE)
		{
			
			$output = $this->load->view('create_user/create_administrators_users' , $data , true);	
			output($output , $data);
		}
		else 
		{
		   $return=$this->user_model->save('add');
		   if(!$return->status)
		   {
			    $userInfo = $this->user_model->get_user_id($this->input->post('user_name'),$this->input->post('user_email'));
				$_POST['user_id'] 		 = $userInfo->user_id;
				$return=$this->user_details_model->save('add');
			    $data['message']  = $return->data;
				$output = $this->load->view('create_user/create_administrators_users' , $data , true); 	
				output($output , $data);
		   }
		   else
		   {
			     $data['message']  = $return->data;
				$output = $this->load->view('create_user/create_administrators_users' , $data , true); 	
				output($output , $data);
		   }
		}	
		
	}
	
		
		
	public function create_representatives_users($user_level = 6)
	{
		
		$data = getCommonData(); 
		$this->load->model("user_model");
		$data['title'] = 'User Entry';
		$data['user_level']=$user_level;
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('first_name', 'First Name', 'max_length[12]|trim|required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'max_length[12]|trim|required');
		$this->form_validation->set_rules('user_name', 'User Name', 'callback_username_check|max_length[12]|alpha_numeric|required');
		$this->form_validation->set_rules('user_pass', 'Password', 'trim|required|matches[passconf]|external_callbacks[model,formval_callbacks,_check_password_strength,TRUE]');
		$this->form_validation->set_rules('passconf', 'Confirm Password', 'trim|required|external_callbacks[model,formval_callbacks,_check_password_strength,TRUE]');
		$this->form_validation->set_rules('user_email', 'Email', 'callback_email_check|required|valid_email');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span>'); 
		$data['action_link']= current_url();
		
		if ($this->form_validation->run() == FALSE)
		{
			
			$output = $this->load->view('create_user/create_representatives_users' , $data , true);	
			output($output , $data);
		}
		else 
		{
		   $return=$this->user_model->save('add');
		   if(!$return->status)
		   {
			    $userInfo = $this->user_model->get_user_id($this->input->post('user_name'),$this->input->post('user_email'));
				$_POST['user_id'] 		 = $userInfo->user_id;
				$this->load->model("user_details_model");
				$return=$this->user_details_model->save('add');
			    $data['message']  = $return->data;
				$output = $this->load->view('create_user/create_representatives_users' , $data , true); 	
				output($output , $data);
		   }
		   else
		   {
			     $data['message']  = $return->data;
				$output = $this->load->view('create_user/create_representatives_users' , $data , true); 	
				output($output , $data);
		   }
		}	
	}//End create_representatives_users()



	public function create_nonrepresentatives_users($user_level = 5)
	{
		
		$data = getCommonData(); 
		$this->load->model("user_model");
		$data['title'] = 'User Entry';
		$data['user_level']=$user_level;
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('first_name', 'First Name', 'max_length[12]|trim|required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'max_length[12]|trim|required');
		$this->form_validation->set_rules('user_name', 'User Name', 'callback_username_check|max_length[12]|alpha_numeric|required');
		$this->form_validation->set_rules('user_pass', 'Password', 'trim|required|matches[passconf]|external_callbacks[model,formval_callbacks,_check_password_strength,TRUE]');
		$this->form_validation->set_rules('passconf', 'Confirm Password', 'trim|required|external_callbacks[model,formval_callbacks,_check_password_strength,TRUE]');
		$this->form_validation->set_rules('user_email', 'Email', 'callback_email_check|required|valid_email');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span>'); 
		$data['action_link']= current_url();
		
		if ($this->form_validation->run() == FALSE)
		{
			
			$output = $this->load->view('create_user/create_nonrepresentatives_users' , $data , true);	
			output($output , $data);
		}
		else 
		{
		   $return=$this->user_model->save('add');
		   if(!$return->status)
		   {
			    $userInfo = $this->user_model->get_user_id($this->input->post('user_name'),$this->input->post('user_email'));
				$_POST['user_id'] 		 = $userInfo->user_id;
				$this->load->model("user_details_model");
				$return=$this->user_details_model->save('add');
			    $data['message']  = $return->data;
				$output = $this->load->view('create_user/create_nonrepresentatives_users' , $data , true); 	
				output($output , $data);
		   }
		   else
		   {
			    $data['message']  = $return->data;
				$output = $this->load->view('create_user/create_nonrepresentatives_users' , $data , true); 	
				output($output , $data);
		   }
		}	
	}//End create_nonrepresentatives_users()
	


	public function create_clients_users($user_level = 3)
	{
		
		$data = getCommonData(); 
		$this->load->model("user_model");
		$data['title'] = 'User Entry';
		$data['user_level']=$user_level;
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('owned_company', 'Owned Company', 'trim|required');
		$this->form_validation->set_rules('purchase_central', 'Purchase Central', 'trim|required');
		$this->form_validation->set_rules('price_central', 'Price Central', 'trim|required');
		$this->form_validation->set_rules('client_type', 'Type Of Client', 'trim|required');
		$this->form_validation->set_rules('shop_type', 'Type Of Shop', 'trim|required');
		$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
		$this->form_validation->set_rules('user_name', 'User Name', 'callback_username_check|max_length[20]|alpha_numeric|required');
		$this->form_validation->set_rules('user_pass', 'Password', 'trim|required|matches[passconf]|external_callbacks[model,formval_callbacks,_check_password_strength,TRUE]');
		$this->form_validation->set_rules('passconf', 'Confirm Password', 'trim|required|external_callbacks[model,formval_callbacks,_check_password_strength,TRUE]');
		$this->form_validation->set_rules('user_email', 'Email', 'callback_email_check|required|valid_email');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span>'); 
		$data['action_link']= current_url();
		
		if ($this->form_validation->run() == FALSE)
		{
			$output = $this->load->view('create_user/create_clients_users' , $data , true);	
			output($output , $data);
		}
		else 
		{
		   $return=$this->user_model->save('add');
		   if(!$return->status)
		   {
			    $userInfo = $this->user_model->get_user_id($this->input->post('user_name'),$this->input->post('user_email'));
				$_POST['user_id'] 		 = $userInfo->user_id;
				$this->load->model("user_details_model");
				$return=$this->user_details_model->save('add');
			    $data['message']  = $return->data;
				$output = $this->load->view('create_user/create_clients_users' , $data , true); 	
				output($output , $data);
		   }
		   else
		   {
			    $data['message']  = $return->data;
				$output = $this->load->view('create_user/create_clients_users' , $data , true); 	
				output($output , $data);
		   }
		}	
	}//End create_clients_users()
	
	
	
	public function username_check($user)
	{				
		$this->load->model("user_model");
		$checkUser = $this->user_model->check_userdb($user);
		if ( ! $checkUser) {
			$this->form_validation->set_message('username_check', 'Username already exist,Choose another one.');
			return FALSE;			
		}
		else {
			return TRUE;
		}				
	}//End username_check()
	
	
	
	public function email_check($email)
	{		
		$this->load->model("user_model");
		$checkUser = $this->user_model->check_emaildb($email);
		if ( ! $checkUser) {
			$this->form_validation->set_message('email_check', 'Email already exist,Choose another one.');
			return FALSE;			
		}
		else {
			return TRUE;
		}				
	}//End email_check()
	
    function list_user($user_level=7)
	{
		$data = getCommonData(); 
		$this->load->model("user_model");
		$data['title'] = 'User Listing';
		$data['user_level']=$user_level;
		$data["users"]=$this->user_model->get_user_list($user_level);
		//echo $this->user_model->lastQuery();
		$output = $this->load->view('create_user/list_users' , $data , true);	
		output($output , $data);
		
	}
	
	
	
	
}//End Class Createusers
