<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalogmaker extends CI_Controller {

	
	
    public function __construct()
    {
        parent::__construct();
		$this->load->model('catalog_maker_model');
		//$checkUserLogin = $this->authentication->check_login(9);
		//if ( ! isset($checkUserLogin->user_level) ){ redirect('/login/', 'refresh');}
		
        // Force SSL
        //$this->force_ssl();		
    }
			
	
	public function index()
	{			
        $data =getCommonData(); 
		$loggedUser = $this->authentication->user_status(9);
		$data['auth_user_id'] = isset($loggedUser->user_id)?$loggedUser->user_id : '';
		$data['auth_user_level'] = isset($loggedUser->user_level)?$loggedUser->user_level : '';
		$data['auth_user_name'] = isset($loggedUser->user_name)?$loggedUser->user_name : '';
		
	
		
		//$output = $this->load->view('create_user/create_administrators_users' , $data , true);
		$output = $this->load->view('create_user/admin_home' , $data , true);
		//print_r($output);
		output($output , $data);
		
	}
	
	public function create_catalog_of_articles()
	{
		$data =getCommonData();
		$output = $this->load->view('catalog_maker/catalog_maker_page' , $data , true);
		output($output , $data);
		//$this->load->view('catalog_maker/catalog_maker_page');
	}//End create_catalog_of_articles()
	
	
	
	public function get_data_by_articleid( $article_id = NULL, $lang = NULL)
	{		
		$artcile_detail = $this->catalog_maker_model->gete_data_by_articleid($article_id, $lang);			
		$data['lang_code'] = $lang;
		if ( $artcile_detail != FALSE) {
			$data['artcile_detail'] = $artcile_detail;
			$this->load->view('catalog_maker/catalog_article_row', $data);		
		}
		else { 
			echo 0;
		}
	}//End get_data_by_articleid()
	
	
}//End Class Catalogmaker
