<?php defined('BASEPATH') or exit('No direct script access allowed');

class Product_article extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("user_model");	
        $this->load->model("user_details_model");		
		$checkUserLogin = $this->authentication->check_login(9);
		if ( ! isset($checkUserLogin->user_level) ){ redirect('/login/', 'refresh');}
		else
			$user_level=$checkUserLogin->user_level;
        // Force SSL
        //$this->force_ssl();	
		
		$this->load->model("user_model");
		$this->load->model("article_model");		
		$this->load->library('form_validation');
		$data['title'] = 'Product Article';	
		
	}
	
	function index($is_non_admin=false)
	{
		$data = getCommonData(); 
		$data['title'] = 'User Listing';
		//$data['user_level']=$user_level;
		$data["articles"]=$this->article_model->findAll();
		//echo $this->user_model->lastQuery();
		$output = $this->load->view('product_article/manage_product_article' , $data , true);	
		output($output , $data);

	}
	
	//Display product_article form.
	//insert record to product_article table
	
	//edit product_article record
	
	function add_article() {
		
		$data = getCommonData(); 
		$this->form_validation->set_rules('article_number', 'Article Number', 'trim|required');
		$this->form_validation->set_rules('ean_code', 'Ean Code', 'trim|required');
		$this->form_validation->set_rules('article_brand', 'Article Brand', 'trim|required');
		$this->form_validation->set_rules('material_type', 'Material Type', 'trim|required');
		$this->form_validation->set_rules('extra_info', 'Extra Info','trim|required');
		$this->form_validation->set_rules('product_name', 'Product Name', 'trim|required');
		$this->form_validation->set_rules('weight', 'Weight', 'trim|required');
		$this->form_validation->set_rules('width', 'Width', 'trim|required');
		$this->form_validation->set_rules('length', 'Length', 'trim|required');
		$this->form_validation->set_rules('article_group', 'Article Group', 'trim|required');
		$this->form_validation->set_rules('design_name', 'Design Name', 'trim|required');
		$this->form_validation->set_rules('colors', 'Color', 'trim|required');
		$this->form_validation->set_rules('unity', 'Unity', 'trim|required');
		$this->form_validation->set_rules('selling_unity', 'Selling Unity', 'trim|required');
		$this->form_validation->set_rules('packing_unity', 'Packing Unity', 'trim|required');
		$this->form_validation->set_rules('price', 'Price', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span>'); 
		$data['action_link']= current_url();
		
		if ($this->form_validation->run() == FALSE)
		{
			$output = $this->load->view('product_article/product_article' , $data , true);	
			output($output , $data);
		}
		else 
		{
		   $return=$this->article_model->save('add');
		   
		   if(!$return->status)
		   {
				$data['message']  = $return->data;
				$output = $this->load->view('product_article/product_article' , $data , true); 	
				output($output , $data);
		   }
		   else
		   {
			     $data['message']  = $return->data;
				$output = $this->load->view('product_article/manage_product_article' , $data , true); 	
				output($output , $data);
		   }
		}		
	}
	function edit_article($product_article_id='') {
		
		$data = getCommonData(); 
		$this->form_validation->set_rules('product_article_id', 'Article ID', 'trim|required');
		
		$this->form_validation->set_rules('article_number', 'Article Number', 'trim|required');
		$this->form_validation->set_rules('ean_code', 'Ean Code', 'trim|required');
		$this->form_validation->set_rules('article_brand', 'Article Brand', 'trim|required');
		$this->form_validation->set_rules('material_type', 'Material Type', 'trim|required');
		$this->form_validation->set_rules('extra_info', 'Extra Info','trim|required');
		$this->form_validation->set_rules('product_name', 'Product Name', 'trim|required');
		$this->form_validation->set_rules('weight', 'Weight', 'trim|required');
		$this->form_validation->set_rules('width', 'Width', 'trim|required');
		$this->form_validation->set_rules('length', 'Length', 'trim|required');
		$this->form_validation->set_rules('article_group', 'Article Group', 'trim|required');
		$this->form_validation->set_rules('design_name', 'Design Name', 'trim|required');
		$this->form_validation->set_rules('colors', 'Color', 'trim|required');
		$this->form_validation->set_rules('unity', 'Unity', 'trim|required');
		$this->form_validation->set_rules('selling_unity', 'Selling Unity', 'trim|required');
		$this->form_validation->set_rules('packing_unity', 'Packing Unity', 'trim|required');
		$this->form_validation->set_rules('price', 'Price', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span>'); 
		$data['action_link']= current_url();
		
		if(!isset($_POST['product_article_id']))
		{
		  $cData=$this->article_model->find(array('product_article_id'=>$product_article_id));
		  if(is_array($cData) && count($cData))
		  {
			 // echo '<pre>';print_r($cData);exit;
			$data=array_merge($data,$cData);
		  }	
		}
		
		
		if ($this->form_validation->run() == FALSE)
		{
			$output = $this->load->view('product_article/product_article' , $data , true);	
			output($output , $data);
		}
		else 
		{
		   $return=$this->article_model->save('edit');
		   
		   if(!$return->status)
		   {
			   $data['message']  = $return->data;
				$output = $this->load->view('product_article/product_article' , $data , true); 	
				output($output , $data);
		   }
		   else
		   {
			     $data['message']  = $return->data;
				$output = $this->load->view('product_article/product_article' , $data , true); 	
				output($output , $data);
		   }
		}		
	}
	
	//delete product_article record
	function delete_article() {
		
	}
	

	
/****************************************
 *  Below are for testing purpose only.
 ***************************************/

}
