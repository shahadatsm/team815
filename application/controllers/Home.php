<?php 
class Home extends CI_Controller {
	function __construct()
	{
		 parent::__construct();
	   //$this->load->helper('language');
	   // $this->lang->load('ic');
	}
	function index($is_non_admin=false)
	{
		
		$data =getCommonData(); 
		
		$output = $this->load->view('home/welcome' , $data , true);
		//print_r($output);
		output($output , $data);		
	}

	function add_user()
	{
		//$this->output->enable_profiler(TRUE);
	    //if(!check_login()) return false;
		$data = getCommonData(); 
		$this->load->model("user_model");
		$data['title'] = 'User Entry';
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('user_name', 'User Name', 'trim|required');
			
		$this->form_validation->set_error_delimiters('<span class="has-error help-block">', '</span>'); 
		$data['action_link']= current_url();//base_url().$this->router->fetch_class()."/addRoadCondition/".$application_id;
		
		if ($this->form_validation->run() == FALSE)
		{
			
			$output = $this->load->view('home/welcome' , $data , true);	
			output($output , $data);
		}
		else 
		{
		   $return = (object)array('status'=>true,'data'=>'');//$this->user_model->save('add');
		   if(!$return->status)
		   {
			    $data['message']  = $return->data;
				$output = $this->load->view('home/welcome' , $data , true); 	
				output($output , $data);
		   }
		   else
		   {
			     $data['message']  = $return->data;
				$output = $this->load->view('home/welcome' , $data , true); 	
				output($output , $data);
		   }
		}		  
	}
	
}
?>