<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	
	
    public function __construct()
    {
        parent::__construct();
        $checkUserLogin = $this->authentication->check_login(9);
		if ( ! isset($checkUserLogin->user_level) ){ redirect('/login/', 'refresh');}
        // Force SSL
        //$this->force_ssl();		
    }
	
	
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{				
	    $data =getCommonData(); 
		
		$output = $this->load->view('welcome_message' , $data , true);
		//print_r($output);
		output($output , $data);
	}
	
	function switch_language($language = "") {
        $language = ($language != "") ? $language : "";
		if(!$language && (isset($_POST['language']) && $_POST['language'])) $language=$_POST['language'];
		$this->session->set_userdata('site_lang', $language);
        $return=array('status'=>true,'data'=>'');
		echo json_encode($return);
		//redirect(base_url());
    }
	
	function home($is_non_admin=false)
	{
		
		$data =getCommonData(); 
		
		$output = $this->load->view('home/welcome' , $data , true);
		//print_r($output);
		output($output , $data);		
	}
}
