$( document ).ready(function() {

	
	$( ".get-article-details" ).click(function() {
	  	//console.log(projectBase);
		$('.catalog-maker-page-show-msg').html("");
		var articleNum = $('#article_id').val();
		var langCode = $('#main_catalog_lang').val();
		console.log(langCode);
		//console.log(articleNum);
		if ( $.trim(langCode).length == 0) {
				$('.catalog-maker-page-show-msg').html('<p class="alert alert-danger">Please Select Main Catalog Language!</p>'); 
		}
		if ( $.trim(articleNum).length != 0 && $.trim(langCode).length != 0 && $.isNumeric(articleNum) ) {
		  $.get(projectBase+'catalogmaker/getarticleinfo/'+articleNum+'/'+langCode, function(data) {
			  console.log(data);
			  if ( data == 0) { 
			  	$('.catalog-maker-page-show-msg').html('<p class="alert alert-danger">Article Not Found!</p>');} 
			  	else { $('#articlelist tr:last').after(data); }		
		  });
		}
	});	
		
});//End $( document ).ready(function()